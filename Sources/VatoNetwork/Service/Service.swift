//  File name   : Service.swift
//
//  Author      : Dung Vu
//  Created date: 1/14/20
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2020 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import Alamofire
import RxSwift

// MARK: - Default
private extension Session {
    static let vatoManger = Session(configuration: API.sessionConfiguration,
                                    serverTrustManager: VATOCustomServerTrustPolicyManager(allHostsMustBeEvaluated: false, evaluators: [:]))
}

extension Session: NetworkServiceProtocol {
    var apiTimeout: Int { return 60 }
    func makeDataRequest(url: URLConvertible,
                         method: HTTPMethod,
                     parameters: Parameters?,
                     encoding: ParameterEncoding,
                     headers: [String: String]?,
                     ignoreCache: Bool = false) throws -> DataRequest
    {
        var headers = headers ?? [:]
        headers["x-app-id"] = API.bundleId
        headers += API.languageHeader
        let h = HTTPHeaders(use: headers)
        let cachePolicy: URLRequest.CachePolicy = ignoreCache ? .reloadIgnoringLocalAndRemoteCacheData : .useProtocolCachePolicy
        let r = URLRequest(url: try url.asURL(), cachePolicy: cachePolicy, timeoutInterval: TimeInterval(self.apiTimeout))
        var new = try encoding.encode(r, with: parameters)
        new.method = method
        if let h = h {
            new.headers = h
        }
        let task = self.request(new)
        return task
    }
    
    
    public func request(use url: URLConvertible,
                 method: HTTPMethod,
                 parameters: Parameters?,
                 encoding: ParameterEncoding,
                 headers: [String: String]?,
                 ignoreCache: Bool) -> Observable<Swift.Result<NetworkResponse, Error>>
    {
        let log = LogResponse.log(url: url, parameters: parameters, header: headers)
        return Observable.create({ (s) -> Disposable in
            let task: Alamofire.DataRequest
            do {
                task = try self.makeDataRequest(url: url,
                                                method: method,
                                                parameters: parameters,
                                                encoding: encoding,
                                                headers: headers,
                                                ignoreCache: ignoreCache)
            } catch {
                s.onError(error)
                return Disposables.create()
            }
            
            task.responseData { data in
                let result = data.result
                defer {
                    s.onCompleted()
                    log(task, data.value, data.error)
                }
                switch result {
                case .success(let value):
                    s.onNext(.success((data.response, value)))
                case .failure(let e):
                    s.onNext(.failure(e))
                }
            }
            task.resume()
            return Disposables.create {
                task.cancel()
            }
        })
            .timeout(RxTimeInterval.seconds(apiTimeout), scheduler: MainScheduler.asyncInstance)
            .catchError { return Observable.just(.failure($0)) }
    }
}


// MARK: - Requester Protocol
public protocol RequestNetworkProtocol {
    var provider: NetworkServiceProtocol { get }
    init(provider: NetworkServiceProtocol)
    func request<T: Decodable>(using router: APIRequestProtocol,
                               decodeTo: T.Type,
                               method: HTTPMethod,
                               encoding: ParameterEncoding,
                               ignoreCache: Bool,
                               block: ((JSONDecoder) -> Void)?) -> Observable<Swift.Result<T, Error>>
}

public extension RequestNetworkProtocol {
    func request<T: Decodable>(using router: APIRequestProtocol,
                               decodeTo: T.Type,
                               method: HTTPMethod = .get,
                               encoding: ParameterEncoding = URLEncoding.default,
                               ignoreCache: Bool = false,
                               block: ((JSONDecoder) -> Void)? = nil) -> Observable<Swift.Result<T, Error>>
    {
        let decoder = JSONDecoder()
        block?(decoder)
        var header = router.header ?? [:]
        if router.path.contains(VatoTicketApi.host) {
            header["token_type"] = "user"
        }
        return provider.request(use: router.path, method: method, parameters: router.params, encoding: encoding, headers: header, ignoreCache: ignoreCache).map { (result)  in
            switch result {
            case .failure(let e):
                return .failure(e)
            case .success(let response):
                return Swift.Result { try decoder.decode(decodeTo, from: response.data) }
            }
        }
    }
}

// MARK: - Requester
public struct NetworkRequester: RequestNetworkProtocol {
    public static let shared = NetworkRequester(provider: Session.vatoManger)
    private (set) public var provider: NetworkServiceProtocol
    public init(provider: NetworkServiceProtocol) {
        self.provider = provider
    }
}

// MARK: - AutoProviderToken
public struct NetworkTokenProvider: NetworkServiceProtocol {
    let provider: NetworkServiceProtocol
    let token: Observable<String>
    
    public init(token: Observable<String>) {
        self.provider = Session.vatoManger
        self.token = token
    }
    
    public init(provider: NetworkServiceProtocol, token: Observable<String>) {
        self.provider = provider
        self.token = token
    }
    
    public func request(use url: URLConvertible, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: [String: String]?, ignoreCache: Bool = false) -> Observable<Swift.Result<NetworkResponse, Error>> {
        var headers = headers ?? [:]
        headers["x-app-id"] = API.bundleId
        headers += API.languageHeader
        return token.take(1).flatMap { (t) -> Observable<Swift.Result<NetworkResponse, Error>> in
            headers["x-access-token"] = t
            headers["Content-Type"] = "application/json; charset=utf-8"
            return self.provider.request(use: url, method: method, parameters: parameters, encoding: encoding, headers: headers, ignoreCache: ignoreCache)
        }
    }
}


// MARK: - Mockup
public struct MockupManagerRequest: NetworkServiceProtocol {
    /// Seconds
    public typealias Response = (_ url: URLConvertible, _ method: HTTPMethod, _ parameters: Parameters?, _ encoding: ParameterEncoding, _ headers: [String: String]?) throws -> NetworkResponse
    public let timeInterval: Int
    public let response: Response
    
    public init(_ timeResponse: Int, response: @escaping Response) {
        self.timeInterval = timeResponse
        self.response = response
    }
    
    public func request(use url: URLConvertible, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: [String: String]?, ignoreCache: Bool = false) -> Observable<Swift.Result<NetworkResponse, Error>> {
        
        return Observable.create { (s) -> Disposable in
            let disposeAble = Observable<Int>.timer(RxTimeInterval.seconds(self.timeInterval), scheduler: MainScheduler.asyncInstance).take(1).bind { (_) in
                s.onNext(Swift.Result { try self.response(url, method, parameters, encoding, headers)})
                s.onCompleted()
            }
            
            return Disposables.create(with: disposeAble.dispose)
        }
    }
}
