//  File name   : Request.swift
//
//  Author      : Dung Vu
//  Created date: 11/19/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import RxSwift
import Alamofire
import RxCocoa

extension HTTPHeaders {
    init?(use headers: [String: String]?) {
        guard let h = headers else { return nil }
        self.init(h)
    }
}

final class VATOCustomServerTrustPolicyManager: ServerTrustManager {
    override func serverTrustEvaluator(forHost host: String) throws -> ServerTrustEvaluating? {
        return try super.serverTrustEvaluator(forHost: host)
        // Only backup revise any time
//        if API.debugging {
//           return try super.serverTrustEvaluator(forHost: host)
//        } else {
//            guard host.contains("vato.vn") else {
//                return try super.serverTrustEvaluator(forHost: host)
//            }
//
//            return PinnedCertificatesTrustEvaluator()
//        }
    }
}


public struct Requester: NetworkProtocol {
    private static let apiTimeout: Int = 60
    private static let manager: Session = Session(configuration: API.sessionConfiguration,
                                                  serverTrustManager: VATOCustomServerTrustPolicyManager(allHostsMustBeEvaluated: false,
                                                                                                         evaluators: [:]))
    
    public static func request(using router: APIRequestProtocol,
                        method m: HTTPMethod = .get,
                        encoding e: ParameterEncoding = URLEncoding.default,
                        progress: ProgressHandler? = nil) -> Observable<(HTTPURLResponse, Data)> {
        let log = LogResponse.log(input: router)
        return Observable.create({ (s) -> Disposable in
            let task: Alamofire.DataRequest
            do {
                task = try manager.makeDataRequest(url: router.path, method: m, parameters: router.params, encoding: e, headers: router.header)
            } catch {
                s.onError(error)
                return Disposables.create()
            }
            
            task.responseData { data in
                defer {
                    log(task, data.value, data.error)
                }
                let result = data.result
                guard let response = data.response else {
                    let e = NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: nil)
                    s.onError(e)
                    return
                }
                switch result {
                case .success(let value):
                    s.onNext((response, value))
                    s.onCompleted()
                case .failure(let e):
                    s.onError(e)
                }
            }
            var disposeProgress: Disposable?
            if let progress = progress {
                var v: Double = 0
                disposeProgress = Observable<Int>.interval(DispatchTimeInterval.milliseconds(300), scheduler: MainScheduler.instance).startWith(0).bind(onNext: { (_) in
                    v += 1
                    let percent = min(v / 100, 1)
                    progress(percent)
                })
            }
            task.resume()
            return Disposables.create {
                disposeProgress?.dispose()
                task.cancel()
            }
        }).timeout(DispatchTimeInterval.seconds(apiTimeout), scheduler: MainScheduler.asyncInstance)
    }
}


