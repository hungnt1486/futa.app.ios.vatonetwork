//  File name   : Ultils.swift
//
//  Author      : Dung Vu
//  Created date: 11/19/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import CoreLocation

typealias JSON = [String: Any]
extension Decodable {
    static func toModel(from data: Data, block: ((JSONDecoder) -> Void)? = nil) throws -> Self {
        let decoder = JSONDecoder()
        let d = data
        // custom
        block?(decoder)
        do {
            let result = try decoder.decode(self, from: d)
            return result
        } catch let err as NSError {
            debugPrint(err)
            throw err
        }
    }
    
    static func toModel(from json: JSON?, block: ((JSONDecoder) -> Void)? = nil) throws -> Self {
        guard let data = try json?.toData() else {
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: [NSLocalizedDescriptionKey: "Not available data!!!"])
        }
        return try self.toModel(from: data, block: block)
    }
}

extension Dictionary {
    func value<E>(for key: Key, defaultValue: @autoclosure () -> E) -> E {
        guard let result = self[key] as? E else {
            return defaultValue()
        }
        return result
    }
    
    func toData() throws -> Data {
        return try JSONSerialization.data(withJSONObject: self, options: [])
    }
}

extension Array {
    func toData() throws -> Data {
        return try JSONSerialization.data(withJSONObject: self, options: [])
    }
}

extension CLLocationCoordinate2D {
    var value: String {
        return "\(self.latitude),\(self.longitude)"
    }
}

extension Dictionary {
    static func +(lhs: Self, rhs: Self) -> Self {
        var result = lhs
        rhs.forEach { (item) in
            result[item.key] = item.value
        }
        return result
    }
    
    static func +=(lhs: inout Self, rhs: Self) {
        let new = lhs + rhs
        lhs = new
    }
}
