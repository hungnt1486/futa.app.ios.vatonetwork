//  File name   : VatoFoodApi.swift
//
//  Author      : Dung Vu
//  Created date: 9/23/19
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2019 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation

@objc
public enum VatoFoodEnvironment: Int {
    case development = 0
    case staging
    case production
}

@objcMembers
public final class VatoFoodApiSettingEnvironment: NSObject {
    private override init() {
        super.init()
    }
    
    public static func set(environment: VatoFoodEnvironment) {
        VatoFoodApi.environment = environment
    }
}


public enum VatoFoodApi: APIRequestProtocol {
    static let pathApi = "/api"
    private static let pathDev = "https://api-ecom-dev.vato.vn"
    private static let pathProduction = "https://api-ecom.vato.vn"
    private static let pathStaging = "https://api-ecom-staging.vato.vn"
    fileprivate static var environment: VatoFoodEnvironment = .development
    
    public static var host: String {
        let result: String
        switch VatoFoodApi.environment {
        case .development:
            result = VatoFoodApi.pathDev
        case .staging:
            result = VatoFoodApi.pathStaging
        case .production:
            result = VatoFoodApi.pathProduction
        }
        return result + VatoFoodApi.pathApi
    }
    
    public static var uploadHost: String {
        let result: String
        switch VatoFoodApi.environment {
        case .development:
            result = VatoFoodApi.pathDev
        case .staging:
            result = VatoFoodApi.pathStaging
        case .production:
            result = VatoFoodApi.pathProduction
        }
        return result
    }
    
    /* nil params if need all category */
    case listCategory(authToken: String, categoryId: Int?, params: [String: Any]?)
    case addCategory(authToken: String, params: [String: Any]?)
    case listBanner(authToken: String, params: [String: Any]?)
    case explore(authToken: String, params: [String: Any]?)
    case nearly(authenToken: String, params: [String: Any]?)
    case searchStore(authenToken: String, params: [String: Any]?)
    case stores(authToken: String, categoryId: Int, params: [String: Any]?)
    case leafCategory(authToken: String, params: [String: Any]?)
    case storeDetail(authToken: String, storeId: Int)
    case suggestKeyword(authToken: String, params: [String: Any]?)
    
    case listMerchant(authToken: String, ownerId: Int, isFull: Bool, params:[String: Any]?)
    case listStoreByMerchant(authToken: String, merchantId: Int, params: [String: Any]?)
    

    case createMerchant(authToken: String, params: [String: Any]?)
    case createUpdateStore(authToken: String, merchantId: Int, param: [String: Any]?)
    case updateMerchant(authToken: String, ownerId: Int, merchantId: Int, params: [String: Any]?)
    case getProductAttributeTemplate(authToken: String, eavEntitySetId: Int)
    case createProduct(authToken: String, params: [String: Any]?)
    case productDetail(authToken: String, productId: Int, params: [String: Any]?)
    case listDisplayProduct(authToken: String, storeId: Int, statusList: String, params: [String: Any]?)
    
    case editOnOffProduct(authToken: String, productId: Int, isOn: Bool, params: [String: Any]?)
    case listMerchantType(authToken: String, params: [String: Any]?)
    
    case updateProduct(authToken: String, storeId: Int, productId: Int, params: [String: Any]?)
    
    case createQuoteCart(authToken: String, params: [String: Any]?)
    case createQuoteCartOffline(authToken: String, params: [String: Any]?)
    case getQuoteCare(authToken: String, params: [String: Any]?)
    case createSaleOrder(authToken: String, quoteId: String, params: [String: Any]?)
    case createSaleOrderOffline(authToken: String, quoteId: String, params: [String: Any]?)
    case getOrder(authToken: String, id: String, params: [String: Any]?)
    case cancelOrder(authToken: String, id: String, params: [String: Any]?)
    case merchantAttributeType(authToken: String, categoryId: String, typeCode: String, params: [String: Any]?)
    case getMerchantPathCategory(authenToken: String, params: [String: Any]?)
    
    case getListSaleOrder(authenToken: String, params: [String: Any]?)
    case getListSaleOrderOffline(authenToken: String, params: [String: Any]?)
    case getSaleOrder(authenToken: String, tripId: String)
    case updateStateOrderOffline(authenToken: String, params: [String: Any]?)
    case listProduct(authenToken: String, params: [String: Any]?)
    
    public var path: String {
        let host = VatoFoodApi.host
        switch self {
        case .listCategory(_, let categoryId, _):
            if let categoryId = categoryId {
                return "\(host)/ecom/category/list-category?category_id=\(categoryId)"
            } else {
                return "\(host)/ecom/category/list-category"
            }
        case .addCategory:
            return "\(host)/ecom/category/add-category"
        case .listMerchant(_, let ownerId, let isFull, _):
            return "\(host)/ecom/merchant?isFull=\(isFull)&ownerId=\(ownerId)"
        case .listStoreByMerchant(_, let merchantId, _):
            return "\(host)/ecom/merchant/\(merchantId)/store"
        case .listBanner:
            return "\(host)/ecom/banner/list-all"
        case .explore:
            return "\(host)/ecom/stores/explore"
        case .nearly:
            return "\(host)/ecom/stores/nearly"
        case .createMerchant(_, _):
            return "\(host)/ecom/merchant"
        case .createUpdateStore(_, let merchantId, _):
            return "\(host)/ecom/merchant/\(merchantId)/stores"
        case .updateMerchant(_, let ownerId, let merchantId, _):
            return "\(host)/ecom/merchant/\(merchantId)?ownerId=\(ownerId)"
        case .searchStore:
            return "\(host)/ecom/discovery/store"
        case .leafCategory:
            return "\(host)/ecom/category/list-leaf-category"
        case .stores(_, let categoryId, _):
            return "\(host)/ecom/stores/category/\(categoryId)"
        case .getProductAttributeTemplate(_, let eavEntitySetId):
            return "\(host)/ecom/product-attribute-template?eavEntitySetId=\(eavEntitySetId)"
        case .createProduct(_, _):
            return "\(host)/ecom/product"
        case .productDetail(_, let productId, _):
            return "\(host)/ecom/product/\(productId)"
        case .storeDetail(_, let storeId):
            return "\(host)/ecom/\(storeId)/store"
        case .listDisplayProduct(_, let storeId, _, _):
            return "\(host)/ecom/discovery/store/\(storeId)/product"
        case .editOnOffProduct(_, let productId, let isOn, _ ):
            return "\(host)/ecom/product/\(productId)/\(isOn ? "on": "off")"
        case .suggestKeyword:
            return "\(host)/ecom/stores/top-text-search"
        case .listMerchantType(_, _):
            return "\(host)/ecom/meta/merchant-type"
        case .updateProduct(_, let storeId, let productId, _):
            return "\(host)/ecom/store/\(storeId)/product/\(productId)"
        case .createQuoteCart(_, _):
            return "\(host)/ecom/quote"
        case .getQuoteCare(_, _):
            return "\(host)/ecom/quote"
        case .createSaleOrder(_, let quoteId, _):
            return "\(host)/ecom/sale-order/\(quoteId)/order"
        case .getOrder(_, let id, _):
            return "\(host)/ecom/sale-order/\(id)/order"
        case .cancelOrder(_, _, _):
            return "\(host)/ecom/sale-order/client-cancel"
        case .merchantAttributeType(_, let categoryId, let typeCode, _):
            return "\(host)/ecom/meta/merchant-attribute-template/category-id/\(categoryId)/type-code/\(typeCode)"
        case .getMerchantPathCategory:
            return "\(host)/ecom/category/list-ancestry"
        case .getListSaleOrder(_):
            return "\(host)/ecom/sale-order"
        case .getSaleOrder(_, let tripId):
            return "\(host)/ecom/driver/sales-order?tripId=\(tripId)"
        case .createQuoteCartOffline(_, _):
            return "\(host)/ecom/quote-offline"
        case .createSaleOrderOffline(_, _, _):
            return "\(host)/ecom/sale-order-offline"
        case .getListSaleOrderOffline(_, _):
            return "\(host)/ecom/sale-order-offline"
        case .updateStateOrderOffline(_, _):
            return "\(host)/ecom/sale-order-offline"
        case .listProduct(_, _):
            return "\(host)/ecom/product"
        }
    }
    
    public var params: [String : Any]? {
        switch self {
        case .listCategory(_, _, let params), .explore(_, let params), .nearly(_, let params):
            return params
        case .addCategory(_, let params):
            return params
        case .listMerchant(_, _, _, let params):
            return params
        case .listStoreByMerchant(_, _, let params):
            return params
        case .listBanner(_, let params):
            return params
        case .createMerchant(_, let params):
            return params
        case .createUpdateStore(_, _, let params):
            return params
        case .updateMerchant(_, _, _, let params):
            return params
        case .searchStore(_, let params):
            return params
        case .leafCategory(_ , let params):
            return params
        case .stores(_, _, let params):
            return params
        case .getProductAttributeTemplate(_,_):
            return nil
        case .createProduct(_, let params):
            return params
        case .productDetail(_, _, let params):
            return params
        case .storeDetail:
            return nil
        case .listDisplayProduct(_, _, _, let params):
            return params
        case .editOnOffProduct(_, _, _, let params):
            return params
        case .suggestKeyword(_, let params):
            return params
        case .listMerchantType(_, let params):
            return params
        case .updateProduct(_, _, _, let params):
            return params
        case .createQuoteCart(_, let params):
            return params
        case .getQuoteCare(_, _):
            return nil
        case .createSaleOrder(_, _, let params):
            return params
        case .getOrder(_, _, _):
            return nil
        case .cancelOrder(_, _, let params):
            return params
        case .merchantAttributeType(_, _, _, let params):
            return params
        case .getMerchantPathCategory(let input):
            return input.params
        case .getListSaleOrder(let input):
            return input.params
        case .getSaleOrder(_, _):
            return nil
        case .createQuoteCartOffline(_, let params):
            return params
        case .createSaleOrderOffline(_, _, let params):
            return params
        case .getListSaleOrderOffline(_, let params):
            return params
        case .updateStateOrderOffline(_, let params):
            return params
        case .listProduct(_ , let params):
            return params
        }
    }
    
    public var header: [String : String]? {
        switch self {
        case .listCategory(let token, _, _), .explore(let token, _), .nearly(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
        case .addCategory(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
        case .listMerchant(let token, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
        case .listStoreByMerchant(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
        case .listBanner(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
        case .createMerchant(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .createUpdateStore(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .updateMerchant(let token, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .searchStore(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .leafCategory(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .stores(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .getProductAttributeTemplate(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .createProduct(let token, _), .storeDetail(let token, _), .suggestKeyword(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .productDetail(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .listDisplayProduct(let token, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .editOnOffProduct(let token, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .listMerchantType(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .updateProduct(let token, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .createQuoteCart(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .getQuoteCare(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .createSaleOrder(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .getOrder(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .cancelOrder(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .merchantAttributeType(let i):
            var h: [String: String] = [:]
            h["x-access-token"] = i.authToken
            h["Content-Type"] = "application/json"
            return h
        case .getMerchantPathCategory(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authenToken
            h["Content-Type"] = "application/json"
            return h
        case .getListSaleOrder(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authenToken
            h["Content-Type"] = "application/json"
            return h
        case .getSaleOrder(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authenToken
            h["Content-Type"] = "application/json"
            return h
        case .createQuoteCartOffline(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .createSaleOrderOffline(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .getListSaleOrderOffline(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authenToken
            h["Content-Type"] = "application/json"
            return h
        case .updateStateOrderOffline(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authenToken
            h["Content-Type"] = "application/json"
            return h
        case .listProduct(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authenToken
            h["Content-Type"] = "application/json"
            return h
        }
    }
    
}
