//
//  MapModel.swift
//  VatoNetwork
//
//  Created by tony on 4/16/19.
//  Copyright © 2019 Dung Vu. All rights reserved.
//

import UIKit

public struct MapModel {
    public struct Place: Codable, Equatable {
        public var primaryName:  String? {
            get {
                if name.isEmpty {
                    return address
                }
                
                return name
            }
        }
        private let name: String
        public var address: String?
        public var location: Location?
        public let placeId: String?
        public let isFavorite: Bool?
        public let distance: Double?
        
        public init(name: String, address: String?, location: Location?, placeId: String?, isFavorite: Bool = false, distance: Double? = nil) {
            self.name = name
            self.address = address
            self.location = location
            self.placeId = placeId
            self.isFavorite = isFavorite
            self.distance = distance
        }
        
        var hashValue: Int {
            return "\(name.lowercased())_\(address?.lowercased() ?? "")".hashValue
        }
        
        public static func == (lhs: Place, rhs: Place) -> Bool {
            return lhs.hashValue == rhs.hashValue
        }
    }
    
    public struct PlaceDetail: Codable {
        public var name: String?
        public var fullAddress: String?
        public var location: Location?
        public let placeId: String?
        public let isFavorite: Bool?
        
        public init(name: String, address: String?, location: Location?, placeId: String? = "", isFavorite: Bool = false) {
            self.name = name
            self.fullAddress = address
            self.location = location
            self.placeId = placeId
            self.isFavorite = isFavorite
        }
    }
    
    public struct Router: Codable {
        public let distance: Double
        public let duration: Double
        public let overviewPolyline: String
    }
    
    public struct Location: Codable {
        public let lat: Double
        public let lon: Double
        
        public init (lat: Double, lon: Double) {
            self.lat = lat
            self.lon = lon
        }
    }
}

