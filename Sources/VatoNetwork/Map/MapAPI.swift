//
//  MapAPI.swift
//  VatoNetwork
//
//  Created by tony on 4/16/19.
//  Copyright © 2019 Dung Vu. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire
import RxSwift

public struct MapAPI {
    public enum Transport: Equatable {
        case walk
        case bike
        case car
        case truck
        
        var mode: String {
            switch self {
            case .bike, .walk:
                return "bike"
            case .car, .truck:
                return "car"
            }
        }
        
        public static func ==(lhs: Transport, rhs: Transport) -> Bool {
            return lhs.mode == rhs.mode
        }
    }
    
    public enum Router: APIRequestProtocol {
        
        case placeSearch(authToken: String, query: String, lat: Double?, lng: Double?)
        
        case placeDetail(authToken: String, placeId: String)
        
        case placeSearchWithDetail(authToken: String, query: String)
        
        case geocoding(authToken: String, lat: Double, lng: Double)
        
        case direction(authToken: String, origin: String, destination: String, transport: Transport)
        
        public var path: String {
            switch self {
            case .placeSearch:
                return "\(API.mapDomain)/placesearch"
                
            case .placeDetail:
                return "\(API.mapDomain)/placedetail"
                
            case .placeSearchWithDetail:
                return "\(API.mapDomain)/findplacefromtext"
                
            case .geocoding:
                return "\(API.mapDomain)/geocode"
                
            case .direction:
                return "\(API.mapDomain)/directions"
            }
        }
        
        public var params: [String: Any]? {
            switch self {
            case .placeSearch(_, let query, let lat, let lng):
                var params = [String: Any]()
                params["query"] = query
                params["lat"] = lat
                params["lon"] = lng
                return params
                
            case .placeDetail(_, let placeId):
                var params = [String: Any]()
                params["placeid"] = placeId
                return params
                
            case .placeSearchWithDetail(_, let query):
                var params = [String: Any]()
                params["query"] = query
                return params
                
            case .geocoding(_, let lat, let lng):
                var params = [String: Any]()
                params["lat"] = lat
                params["lon"] = lng
                return params
                
            case .direction(_, let origin, let destination, let transport):
                var params = [String: Any]()
                params["origin"] = origin
                params["destination"] = destination
                params["transport"] = transport.mode
                return params
            }
        }
        
        public var header: [String: String]? {
            switch self {
            case .placeSearch(let authToken, _, _, _):
                var h: [String: String] = [:]
                h["x-access-token"] = authToken
                h["Content-Type"] = "application/json"
                return h
                
            case .placeDetail(let authToken, _):
                var h: [String: String] = [:]
                h["x-access-token"] = authToken
                h["Content-Type"] = "application/json"
                return h
                
            case .placeSearchWithDetail(let authToken, _):
                var h: [String: String] = [:]
                h["x-access-token"] = authToken
                h["Content-Type"] = "application/json"
                return h
                
            case .geocoding(let authToken, _, _):
                var h: [String: String] = [:]
                h["x-access-token"] = authToken
                h["Content-Type"] = "application/json"
                return h
                
            case .direction(let authToken, _, _, _):
                var h: [String: String] = [:]
                h["x-access-token"] = authToken
                h["Content-Type"] = "application/json"
                return h
            }
        }
    }
}


// MARK: - REST Request
extension MapAPI {
    public static func findPlace(with keyword: String, currentLocation: CLLocationCoordinate2D, authToken: String) -> Observable<[MapModel.Place]> {
        let router = MapAPI.Router.placeSearch(authToken: authToken, query: keyword, lat: currentLocation.latitude, lng: currentLocation.longitude)
        
        return Requester.responseDTO(decodeTo: OptionalMessageDTO<[MapModel.Place]>.self, using: router)
            .flatMap({ (data) -> Observable<[MapModel.Place]> in
                if let d = data.response.data {
                    return Observable.just(d)
                }
                return Observable.empty()
            })
    }

    public static func placeDetails(with placeId: String, authToken: String) -> Observable<MapModel.PlaceDetail> {
        let router = MapAPI.Router.placeDetail(authToken: authToken, placeId: placeId)
        return Requester.responseDTO(decodeTo: OptionalMessageDTO<MapModel.PlaceDetail>.self, using: router)
            .flatMap({ (data) -> Observable<MapModel.PlaceDetail> in
                if let d = data.response.data {
                    return Observable.just(d)
                }
                return Observable.empty()
        })
    }
    
    public static func geocoding(authToken: String, lat: Double, lng: Double) -> Observable<MapModel.PlaceDetail> {
        let router = MapAPI.Router.geocoding(authToken: authToken, lat: lat, lng: lng)
        return Requester.responseDTO(decodeTo: OptionalMessageDTO<MapModel.PlaceDetail>.self, using: router)
            .flatMap({ (data) -> Observable<MapModel.PlaceDetail> in
                if let d = data.response.data {
                    return Observable.just(d)
                }
                return Observable.empty()
        })
    }
    
    public static func findDirection(authToken: String, origin: String, destination: String, transport: Transport) -> Observable<MapModel.Router>{
        let router = MapAPI.Router.direction(authToken: authToken, origin: origin, destination: destination, transport: transport)
        return Requester.responseDTO(decodeTo: OptionalMessageDTO<MapModel.Router>.self, using: router)
        .flatMap({ (data) -> Observable<MapModel.Router> in
            if let d = data.response.data {
                return Observable.just(d)
            }
            return Observable.empty()
        })
    }
}
