//  File name   : Domain.swift
//
//  Author      : Dung Vu
//  Created date: 11/19/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
@objcMembers
public class API: NSObject {
    internal static var debugging: Bool = false
    public static var logAPI: Bool = true
    internal static let googleDomain = "https://maps.googleapis.com"
    internal static var customPath: String?
    internal static var port: Int = 9000
    private static var _sessionConfiguration: Foundation.URLSessionConfiguration?
    
    internal static var sessionConfiguration: Foundation.URLSessionConfiguration {
        return _sessionConfiguration ?? .default
    }
    
    internal static let bundleId: String = {
        let result: String = Bundle.main.infoDictionary?.value(for: "CFBundleIdentifier", defaultValue: "") ?? ""
        precondition(!result.isEmpty, "failed bundle")
        return result
    }()
    
    internal static let appVersion: String = {
        let result: String = Bundle.main.infoDictionary?.value(for: "CFBundleShortVersionString", defaultValue: "") ?? ""
        precondition(!result.isEmpty, "failed bundle")
        return result
    }()
    
    internal static let languageHeader: [String: String] = {
        var r = [String: String]()
        let code = Locale.current.languageCode
        let lang = code?.lowercased().contains("vi") == true ? "vi" : "en"
        r["x-app-version"] = appVersion
        r["Accept-Language"] = lang
        return r
    }()
    
    public static func setCustom(configuration: Any) {
        guard let configuration = configuration as? URLSessionConfiguration else { return }
        self._sessionConfiguration = configuration
    }
    
    /// Config for Debug Mode
    ///
    /// - Parameter debug: true: Debug false: Production
    public static func config(use debug: Bool) {
        debugging = debug
    }
    
    /// Change Port For Debug Mode
    ///
    /// - Parameter port: 11000: driver 9000: client
    public static func changePort(modeDev port: Int) {
        self.port = port
    }
    
    
    /// Use for log api
    /// - Parameter debug: default is true
    public static func logApi(use debug: Bool) {
        logAPI = debug
    }
    
    /// Set custom debug mode with custom url
    ///
    /// - Parameter pathURL: full url path with scheme
    public static func customPathForDebug(pathURL: String) {
        self.customPath = pathURL
    }
    
    private override init() {
        super.init()
    }
    
    internal static let vatoDomain: String = {
        let pathDev = customPath ?? "https://api-dev.vato.vn/api"
        return debugging ? pathDev : "https://api.vato.vn/api"
    }()
    
    internal static let taxiDomain: String = {
        let pathDev = customPath ?? "https://api-dev.vato.vn/taxi"
        return debugging ? pathDev : "https://api.vato.vn/taxi"
    }()
    
    internal static let mapDomain: String = {
        let pathDev = "https://map-dev.vato.vn/api"
        return debugging ? pathDev : "https://map.vato.vn/api"
    }()
}
