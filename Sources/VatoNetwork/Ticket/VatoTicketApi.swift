//  File name   : VatoTicketApi.swift
//
//  Author      : Dung Vu
//  Created date: 9/23/19
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2019 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation

public enum VatoTicketApi: APIRequestProtocol {
    private static let pathDev = "https://api-busline-dev.vato.vn/api"
    private static let pathProduction = "https://api-busline.vato.vn/api"
    
    public static var host: String {
        return API.debugging ? VatoTicketApi.pathDev : VatoTicketApi.pathProduction
    }
    
    case listOriginPoint(authToken: String)
    case getRouteBetweenTwoPoint(authToken: String, orginCode: String, desCode: String, departureDate: String)
    case buslinesSchedules(authToken: String, routeId: Int32, departureDate: String)
    case listStop(authToken: String, routeId: Int, departureDate: String, departureTime: String, wayId: Int32)
    case listSeat(authToken: String, routeId: Int, carBookingId: Int, kind: String, departureDate: String, departureTime: String)
    case saveSeat(authToken: String, param: SaveSeatParamModel)
    case checkQRCode(authToken: String, qrCode: String)
    case payment(authToken: String, ticketsCodes: [String], userId: Int64, method: Int, description: String, cardId: Int, deviceId: String)
    case listTicket(authToken: String, params: [String: Any])
    case ticketDetail(authToken: String, code: String)
    case cancelTicket(authToken: String, ticketsCode: String, userId: Int64, status: Int)
    case changeTicket(authToken: String, newCode: String, oldCode: String, userId: Int64, method: Int, description: String)
    case price(authToken: String, price: Double, paymentMethod: Int)
    
    public var path: String {
        switch self {
        case .listOriginPoint:
            return "\(VatoTicketApi.host)/buslines/futa/routes/codes"
        case .getRouteBetweenTwoPoint(_, let orginCode, let desCode, _):
            return "\(VatoTicketApi.host)/buslines/futa/routes/prices/\(orginCode)/\(desCode)"
        case .buslinesSchedules(_, let routeId, let departureDate):
            return "\(VatoTicketApi.host)/buslines/futa/drivers/tables/\(routeId)?departureDate=\(departureDate)"
        case .listStop(_, let routeId, let departureDate, let departureTime, let wayId):
            return "\(VatoTicketApi.host)/buslines/futa/routes/stops/\(routeId)?departureDate=\(departureDate)&departureTime=\(departureTime)&wayId=\(wayId)"
        case .listSeat(_, let routeId, let carBookingId, let kind, let departureDate, let departureTime):
            return "\(VatoTicketApi.host)/buslines/futa/seats/\(routeId)/\(carBookingId)?kind=\(kind)&departureDate=\(departureDate)&departureTime=\(departureTime)"
        case .saveSeat(_, _):
            return "\(VatoTicketApi.host)/buslines/futa/ticket"
        case .checkQRCode(_, _):
            return "\(VatoTicketApi.host)/express/futa/tickets/promotion"
        case .payment(_, _, _, _, _, _, _):
            return "\(VatoTicketApi.host)/buslines/futa/ticket/checkout"
        case .listTicket(_, _):
            return "\(VatoTicketApi.host)/buslines/futa/ticket/"
        case .ticketDetail(_, let code):
            return "\(VatoTicketApi.host)/buslines/futa/ticket/\(code)"
        case .cancelTicket(_, let ticketsCode, let userId, let status):
            return "\(VatoTicketApi.host)/buslines/futa/ticket/\(ticketsCode)/status/\(status)?userId=\(userId)"
        case .changeTicket(_, _, _, _, _, _):
            return "\(VatoTicketApi.host)/buslines/futa/ticket/change"
        case .price(_, let price, let payMethod):
             return "\(VatoTicketApi.host)/buslines/futa/prices?price=\(Int64(price))&payment_method=\(payMethod)"
        }
    }
    
    public var params: [String : Any]? {
        switch self {
        case .listOriginPoint(_):
            return nil
        case .getRouteBetweenTwoPoint(_, _, _, let departureDate):
            return ["departureDate": departureDate]
        case .buslinesSchedules:
            return nil
        case .listStop(_, _, _, _, _):
            return nil
        case .listSeat(_, _, _, _, _, _):
            return nil
        case .saveSeat(_, let param):
            return param.toJson()
        case .checkQRCode(_, let qrCode):
            return [
                "barcode": qrCode
            ]
        case .payment(_, let ticketCodes, let userId, let method, let description, let cardId, let deviceId):
            return [
                "userId": userId,
                "method": method,
                "description": description,
                "ticketCodes": ticketCodes,
                "cardId": cardId,
                "deviceId": deviceId
            ]
        case .listTicket(_, let param):
            return param
        case .ticketDetail(_, _):
            return nil
        case .cancelTicket(_, _, _, _):
            return nil
        case .changeTicket(_, let newCode, let oldCode, let userId, let method, let description):
            return [
                "userId": userId,
                "method": method,
                "description": description,
                "oldCode": oldCode,
                "newCode": newCode
            ]
        case .price(_, _, _):
            return nil
        }
    }
    
    public var header: [String : String]? {
        var headers: [String: String]
        switch self {
        case .listOriginPoint(let token):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            headers = h
        case .getRouteBetweenTwoPoint(let token, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            headers = h
        case .buslinesSchedules(let token, _, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
            headers = h
        case .listStop(let token, _, _, _, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
            headers = h
        case .listSeat(let token, _, _, _, _, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
            headers = h
        case .saveSeat(let token, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
            headers = h
        case .checkQRCode(let token, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
            headers = h
        case .payment(let token, _, _, _, _, _, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
        
            headers = h
        case .listTicket(let token, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
            
            headers = h
        case .ticketDetail(let token, _):
            let h: [String:String] = [
                "x-access-token": token
            ]
            
            headers = h
        case .cancelTicket(let authToken, _, _, _):
            let h: [String:String] = [
                "x-access-token": authToken
            ]
            
            headers = h
        case .changeTicket(let authToken, _, _, _, _, _):
            let h: [String:String] = [
                "x-access-token": authToken
            ]
            
            headers = h
        case .price(let authToken, _, _):
            let h: [String:String] = [
                "x-access-token": authToken
            ]
            
            headers = h
        }
        headers["token_type"] = "user"
        return headers
    }
}
