//  File name   : VatoApi.swift
//
//  Author      : Dung Vu
//  Created date: 11/19/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import CoreLocation

public enum VatoAPIRouter: APIRequestProtocol {
    case getBalance(authToken: String)
    case searchDriver(authToken: String, coordinate: CLLocationCoordinate2D, service: Int)
    
    // Promotion
    case promotion(authToken: String, code: String)
    case promotionCancel(authToken: String, promotionToken: String)
    case promotionList(authToken: String, coordinate: CLLocationCoordinate2D?)
    case promotionDetail(authToken: String, promotionId: Int)
    case promotionSearch(authToken: String, code: String)
    case promotionNow(authToken: String, zoneId: Int)
    
    case updateDeviceToken(authToken: String, firebaseID: String, phoneNumber: String, deviceToken: String, appVersion: String)     // FIX FIX FIX: The same as update account
    
    // Driver
    case listBank
    case checkPin(authToken: String)
    case addBankInfo(authToken: String, bankCode: Int, bankAccount: String, accountName: String, identityCard: String, pin: String)
    case listBankInfos(authToken: String)
    case orderWithdraw(authToken: String, bankInfoId: Int, amount: Int, pin: String)
    case driverWithdrawInfor(authToken: String)
    case driverUpdateOnlineStatus(authToken: String, status: DriverOnlineParam)
    
    // Authenticate
    case authenticateConfig
    case authenticateRequestCode(phone: String, deviceId: String)
    case authenticateVerifyCode(sms: String, sessionId: String)
    
    //Register
    case checkAccount(authToken: String, firebaseID: String)
    case createAccount(authToken: String, firebaseID: String, phoneNumber: String, deviceToken: String?, fullName: String?, nickname: String?, email: String?, birthday: TimeInterval?, zoneID: Int?, avatarURL: String?, referralCode: String?)
    case updateAccount(authToken: String, firebaseID: String, phoneNumber: String, password: String? = nil ,deviceToken: String?, fullName: String?, nickname: String?, email: String?, birthday: TimeInterval?, zoneID: Int?, avatarURL: String?)
    case checkPhone(authToken: String, firebaseID: String, phoneNumber: String)
    case checkReferralCode(authToken: String, referralCode: String)
    
    //Top up
    case topUp(authToken: String, amount: Int, appId: Int)
    case topUpMomo(authToken: String, transId: String, amount: Int, appId: Int, customerNumber: String, appData: String)
    case topUpNapas(authToken: String, params: [String: Any]?)
    case topUpDomesticAtm(authToken: String, params: [String: Any]?)
    ///user/transactions
    
    // Transaction History
    case userTransactions(authToken: String, fromDate: UInt64, toDate: UInt64, page: Int, size: Int, balanceType: Int)
    case userTopupConfig(authToken: String)
    case userTransfer(authToken: String, phone: String?, amount: Int, pin: String)
    case userDetailTransaction(authToken: String, id: Int)
    
    // Trip History
    case bookingHistory(token: String, param: [String: Any])
    
    // Blacklist / Favorite drivers
    case getUserInfo(token: String, phoneNumber: String)
    case addDriverToBlacklist(token: String, userID: Int64)
    case addDriverToFavorite(token: String, userID: Int64)
    case removeDriverFromBlacklist(token: String, userID: Int64)
    case removeDriverFromFavorite(token: String, userID: Int64)
    
    //Booking
    case findDriverForBook(authToken: String,
        distance: Double,
        fare: Double,
        isFavorite: Bool,
        originCoor: CLLocationCoordinate2D,
        destinationCoor: CLLocationCoordinate2D?,
        page: Int,
        service: Int,
        size: Int)
    
    //Referral: http://api-dev.vato.io/api/user/referral_info
    case referralInfo(authToken: String, userId: Int)
    
    // Fare setting
    /// - parameters:
    ///   - version: 1. Old data to backward compatible with current version.
    ///              2. New data to support from version 5.9.0.
    ///              Default value if null is 1
    case fareSetting(authToken: String, origin: String, destination: String?, version: Int?)
    
    //list card
    case listCard(authToken: String)
    
    case removeCard(authToken: String, userId: String, tokenId: String)
    
    case payUserDebt(authToken: String, payment: Int, cardID: String?, tripIDs: [String])
    
    case getUserDebt(authToken: String)
    
    // Favorite place type
    
    case getFavPlaceType(authToken: String)
    case getFavPlaceList(authToken: String, isDriver: Bool)
    case createFavPlace(authToken: String, name: String, address: String, typeId: Int, lat: String, lon: String, isDriver: Bool)
    case deleteFavPlace(authToken: String, placeId: Int64)
    case updateFavPlace(authToken: String, placeId: Int64, name: String, address: String, typeId: Int, lat: String, lon: String, isDriver: Bool)
    
    case driverActiveFavMode(authToken: String, placeId: Int64, coordinate: CLLocationCoordinate2D)
    case driverTurnOffFavMode(authToken: String, activeId: Int64, tripId: String)
    case driverChangPlaceFavMode(authToken: String, activeId: Int64, placeId: Int64)
    case getStatusFavMode(authToken: String)
    
    // MARK: GroupService
    case getListServices(authToken: String, sericeGroup: String?)
    case getFaresServices(authToken: String, param: FareServiceParam)
    
    // MARK: Find driver information
    case findDriverInformation(authToken: String, userId: String)
    case getDetailTrip(authToken: String, tripId: String)
    case updateFCMToken(authToken: String, fcmToken: String, driver: Bool)
    
    case customPath(authToken: String, path: String, header: [String: String]?, params: [String: Any]?, useFullPath: Bool)
    
    case getUserNotification(authToken: String, params: [String: Any]?)
    
    case getMasterConfigs(authToken: String, groupServiceId: Int, type: String, app: String)
    
    case feedback(token: String, params: [String: Any]?)
    case createSupport(token: String, params: [String: Any]?)
    case answerFeedback(token: String, supportId: String, params: [String: Any]?)
    case feedbackUpdateStatus(token: String, supportId: String, params: [String: Any]?)
    
    
    // Taxi Api
    case pickupStations(token: String, params: [String: Any]?)
    case getPickupStationDriver(token: String, pickupId: Int, params: [String: Any]?)
    case removeDriverOrderTaxiRequest(token: String, stationId: Int, driverId: Int, params: [String: Any]?)
    case getDriverNearby(token: String, stationId: Int, params: [String: Any]?)
    case getRequestedDriver(token: String, stationId: Int, params: [String: Any]?)
    case updateDriverRequest(token: String, params: [String: Any]?)
    
    public var path: String {
        switch self {
        case .getBalance:
            return "\(API.vatoDomain)/balance/get"
            
        case .searchDriver:
            return "\(API.vatoDomain)/user/search"
            
        case .promotion:
            return "\(API.vatoDomain)/promotion/apply_code"
            
        case .promotionCancel:
            return "\(API.vatoDomain)/promotion/cancel_promotion_token"
            
        case .promotionList:
            return "\(API.vatoDomain)/promotion/list_promotion"
            
        case .promotionDetail:
            return "\(API.vatoDomain)/manifest/get"
            
        case .promotionSearch:
            return "\(API.vatoDomain)/promotion/search"
            
        case .promotionNow:
            return "\(API.vatoDomain)/manifest/now"
            
        case .updateDeviceToken:
            return "\(API.vatoDomain)/user/update_account"
            
        case .listBank:
            guard
                let domain = URL(string: API.vatoDomain),
                let scheme = domain.scheme,
                let host = domain.host
                else {
                    fatalError("Invalid url.")
            }
            
            var port = ""
            if let p = domain.port {
                port = ":\(p)"
            }
            
            return "\(scheme)://\(host)\(port)/m/bank_withdraw_support"
            
        case .checkPin:
            return "\(API.vatoDomain)/user/check_pin"
            
        case .listBankInfos:
            return "\(API.vatoDomain)/user/get_user_bank_info"
            
        case .addBankInfo:
            return "\(API.vatoDomain)/user/add_bank_info"
            
        case .orderWithdraw:
            return "\(API.vatoDomain)/balance/add_withdraw"
            
        case .authenticateRequestCode:
            return "\(API.vatoDomain)/authenticate/request_code"
            
        case .authenticateVerifyCode:
            return "\(API.vatoDomain)/authenticate/verify_code"
            
        case .authenticateConfig:
            return "\(API.vatoDomain)/authenticate/master_config"
            
        case .checkAccount:
            return "\(API.vatoDomain)/user/check_account"
            
        case .createAccount:
            return "\(API.vatoDomain)/user/create_account"
            
        case .updateAccount:
            return "\(API.vatoDomain)/user/update_account"
            
        case .checkPhone:
            return "\(API.vatoDomain)/user/check_phone"
            
        case .checkReferralCode:
            return "\(API.vatoDomain)/user/check_referral_code"
            
        case .topUp:
            return "\(API.vatoDomain)/balance/zalopay/create_topup_order"
            
        case .topUpMomo:
            return "\(API.vatoDomain)/balance/momopay/create_topup_order"
            
        case .topUpNapas:
            return "\(API.vatoDomain)/balance/napas/purchase_international_token"
            
        case .topUpDomesticAtm:
            return "\(API.vatoDomain)/balance/napas/purchase_domestic_token"
        case .driverWithdrawInfor:
            return "\(API.vatoDomain)/user/get_user_withdraw_info"
            
        case .userTransactions:
            return "\(API.vatoDomain)/balance/transactions"
            
        case .userTopupConfig:
            return "\(API.vatoDomain)/user/get_user_topup_info"
            
        case .userTransfer:
            return "\(API.vatoDomain)/user/transfer"
            
        case .userDetailTransaction:
            return "\(API.vatoDomain)/balance/get_transaction"
            
        case .findDriverForBook:
            return "\(API.vatoDomain)/user/search_driver_v2"
            
        case .bookingHistory:
            return "\(API.vatoDomain)/trip/list_client"
            
        case .getUserInfo:
            return "\(API.vatoDomain)/user/check_info"
            
        case .addDriverToBlacklist:
            return "\(API.vatoDomain)/user/add_driver_to_blacklist"
            
        case .addDriverToFavorite:
            return "\(API.vatoDomain)/user/add_to_favorite"
            
        case .removeDriverFromBlacklist:
            return "\(API.vatoDomain)/user/remove_driver_from_blacklist"
            
        case .removeDriverFromFavorite:
            return "\(API.vatoDomain)/user/remove_from_favorite"
            
        case .referralInfo:
            return "\(API.vatoDomain)/referral"
            
        case .fareSetting:
            return "\(API.vatoDomain)/trip/fare/settings"
            
        case .listCard:
            return "\(API.vatoDomain)/balance/napas/list_token"
            
        case .removeCard:
            return "\(API.vatoDomain)/balance/napas/drop_token"
            
        case .payUserDebt:
            return "\(API.vatoDomain)/user/pay_debt"
            
        case .getUserDebt:
            return  "\(API.vatoDomain)/user/debt_info"
        case .getFavPlaceType:
            return  "\(API.vatoDomain)/favorite_places_type"
        case .createFavPlace:
            return  "\(API.vatoDomain)/favorite_place"
        case .getFavPlaceList:
            return  "\(API.vatoDomain)/favorite_place"
        case .deleteFavPlace(_, let placeId):
            return  "\(API.vatoDomain)/favorite_place/\(placeId)"
        case .updateFavPlace(_, let placeId, _, _, _, _, _, _):
            return  "\(API.vatoDomain)/favorite_place/\(placeId)"
        case .driverActiveFavMode(_, let placeId, _):
            return  "\(API.vatoDomain)/driver_favorite_activate/\(placeId)"
        case .driverTurnOffFavMode(_, let activeId, let tripId):
            return  "\(API.vatoDomain)/driver_favorite_activate/\(activeId)?trip=\(tripId)"
        case .getStatusFavMode(_):
            return  "\(API.vatoDomain)/driver_favorite_activate"
        case .driverChangPlaceFavMode(_, let activeId, let placeId):
            return  "\(API.vatoDomain)/driver_favorite_activate/\(activeId)/\(placeId)"
        case .getListServices(_, let sericeGroup):
            if let sericeGroup = sericeGroup,
                sericeGroup.isEmpty == false {
                return  "\(API.vatoDomain)/products/\(sericeGroup)/services"
            }
            return  "\(API.vatoDomain)/products/services"
        case .getFaresServices(_, _):
            return  "\(API.vatoDomain)/products/services/fares"
        case .findDriverInformation(_ , let userId):
            return  "\(API.vatoDomain)/driver/\(userId)"
        case .driverUpdateOnlineStatus:
            return "\(API.vatoDomain)/driver/update_status"
        case .getDetailTrip:
            return "\(API.vatoDomain)/trip/trip_detail"
        case .updateFCMToken:
            return "\(API.vatoDomain)/user/device_tokens"
            
        case .customPath(_ , let path, _, _, let usePath):
//            assert(!path.contains("/api"), "Please remove api/ first")
            return usePath ? path : "\(API.vatoDomain)/\(path)"
        case .getUserNotification:
            return "\(API.vatoDomain)/notification/list_for_user"
        case .getMasterConfigs(let input):
            return "\(API.vatoDomain)/master-configs/\(input.app)/\(input.type)/\(input.groupServiceId)"
        case .feedback(_):
            return "\(API.vatoDomain)/support/review"
        case .createSupport(_):
            return "\(API.vatoDomain)/support/feedbacks"
        case .answerFeedback(let input):
            return "\(API.vatoDomain)/support/feedbacks/\(input.supportId)/comments"
        case .feedbackUpdateStatus(let input):
            return "\(API.vatoDomain)/support/feedbacks/\(input.supportId)/comments/status"
            
                        
        case .pickupStations(_):
            return "\(API.taxiDomain)/pickup-stations"
        case .getPickupStationDriver(let input):
            return "\(API.taxiDomain)/pickup-stations/\(input.pickupId)/drivers"
        case .removeDriverOrderTaxiRequest(let input):
            return "\(API.taxiDomain)/pickup-stations/\(input.stationId)/registrations/\(input.driverId)"
        case .getDriverNearby(let input):
            return "\(API.taxiDomain)/operators/pickup-stations/\(input.stationId)/nearby-drivers"
        case .getRequestedDriver(let input):
            return "\(API.taxiDomain)/operators/\(input.stationId)/registrations"
        case .updateDriverRequest:
            return "\(API.taxiDomain)/operators/registrations"
        }
    }
    
    public var params: [String: Any]? {
        switch self {
        case .getBalance:
            return nil
            
        case .searchDriver(_, let coordinate, let service):
            return [
                "lat": coordinate.latitude,
                "lon": coordinate.longitude,
                "distance": 5,
                "service": service,
                "isFavorite": false,
                "page": 0,
                "size": 10
            ]
            
        case .promotion(_, let code):
            return ["code": code
            ]
            
        case .promotionCancel(_, let promotionToken):
            return ["promotionToken":promotionToken]
            
        case .promotionList(_, let coordinate):
            if let coordinate = coordinate {
                return ["lat": coordinate.latitude, "lon": coordinate.longitude]
            } else {
                return nil
            }
            
        case .promotionDetail(_ , let promotionId):
            return ["id": promotionId]
            
        case .promotionSearch( _, let code):
            return ["code": code]
            
        case .promotionNow(_ , let zoneId):
            return ["zoneId": zoneId]
            
        case .updateDeviceToken(_, let firebaseID, let phoneNumber, let deviceToken, let appVersion):
            return [
                "phoneNumber":phoneNumber,
                "firebaseId":firebaseID,
                "deviceToken":deviceToken,
                "appVersion":appVersion
            ]
            
        case .listBank:
            return nil
            
        case .checkPin:
            return nil
            
        case .addBankInfo(_, let bankCode, let bankAccount, let accountName, let identityCard, let pin):
            return [
                "bankCode":bankCode,
                "bankAccount":bankAccount,
                "accountName":accountName,
                "identityCard": identityCard,
                "pin":pin
            ]
            
        case .listBankInfos:
            return nil
            
        case .orderWithdraw(_, let bankInfoId, let amount, let pin):
            return [
                "bankInfoId":bankInfoId,
                "amount":amount,
                "pin":pin
            ]
            
        case .checkAccount(_, let firebaseID):
            return [
                "firebaseId":firebaseID
            ]
        case .createAccount(_, let firebaseID, let phoneNumber, let deviceToken, let fullName, let nickname, let email, let birthday, let zoneID, let avatarURL, let referralCode):
            var params: [String: Any] = [
                "firebaseId":firebaseID,
                "phoneNumber":phoneNumber,
                "isDriver":false
            ]
            params["appVersion"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            params["deviceToken"] = deviceToken
            params["fullName"] = fullName
            params["nickname"] = nickname
            params["email"] = email
            params["birthday"] = birthday
            params["zoneId"] = zoneID
            params["avatarUrl"] = avatarURL
            params["referralCode"] = referralCode
            return params
            
        case .updateAccount(_, let firebaseID, let phoneNumber, let password, let deviceToken, let fullName, let nickname, let email, let birthday, let zoneID, let avatarURL):
            var params: [String: Any] = [
                "firebaseId":firebaseID,
                "phoneNumber":phoneNumber,
                "isDriver":false
            ]
            params["appVersion"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            params["deviceToken"] = deviceToken
            params["fullName"] = fullName
            params["nickname"] = nickname
            params["password"] = password
            params["email"] = email
            params["birthday"] = birthday
            params["zoneId"] = zoneID
            params["avatarUrl"] = avatarURL
            return params
            
        case .checkPhone(_, let firebaseID, let phoneNumber):
            let params: [String: Any] = [
                "firebaseId":firebaseID,
                "phoneNumber":phoneNumber,
            ]
            return params
            
        case .checkReferralCode(_, let referralCode):
            let params: [String: Any] = ["referralCode":referralCode]
            return params
            
        case .authenticateRequestCode(let phone, let deviceId):
            return ["phoneNumber": phone, "deviceId": deviceId]
            
        case .authenticateVerifyCode(let sms, let sessionId):
            return ["verifyCode": sms, "sessionId": sessionId]
            
        case .authenticateConfig:
            return nil
            
        case .topUp(_ , let amount, let appId):
            return ["amount": amount, "appid": appId]
            
        case .topUpMomo(_, let transId, let amount, let appId, let customerNumber, let appData):
            return ["transId" : transId, "amount": amount, "appid": appId, "customerNumber": customerNumber, "appData": appData]
            
        case .topUpNapas(_, let params):
            return params
            
        case .topUpDomesticAtm(_, let params):
            return params
        case .driverWithdrawInfor:
            return nil
            
        case .userTransactions(_, let fromDate, let toDate, let page, let size, let balanceType):
            return ["from": fromDate, "to": toDate, "page": page, "size": size, "balanceType" : balanceType]
            
        case .userTopupConfig:
            return nil
            
        case .userTransfer(_ ,let phone, let amount, let pin):
            var p = [String: Any]()
            p["amount"] = amount
            p["phone"] = phone
            p["pin"] = pin
            return p
            
        case .userDetailTransaction(_ , let id):
            return ["id": id]
            
        case .findDriverForBook(_, let distance, let fare, let isFavorite, let originCoor, let destinationCoor, let page, let service, let size):
            if let destinationCoor = destinationCoor {
                return ["distance": distance,
                        "fare": fare,
                        "isFavorite": isFavorite,
                        "origin": "\(originCoor.latitude),\(originCoor.longitude)",
                    "destination": "\(destinationCoor.latitude),\(destinationCoor.longitude)",
                    "page": page,
                    "service": service,
                    "size": size]
            } else {
                return ["distance": distance,
                        "fare": fare,
                        "isFavorite": isFavorite,
                        "origin": "\(originCoor.latitude),\(originCoor.longitude)",
                    "page": page,
                    "service": service,
                    "size": size]
            }
            
        case .bookingHistory(_, let params):
            return params
            
        case .getUserInfo(_, let phoneNumber):
            return ["phoneNumber":phoneNumber]
            
        case .addDriverToBlacklist(_, let userID):
            return ["userId":userID]
            
        case .addDriverToFavorite(_, let userID):
            return ["driverId":userID]
            
        case .removeDriverFromBlacklist(_, let userID):
            return ["userId":userID]
            
        case .removeDriverFromFavorite(_, let userID):
            return ["driverId":userID]
            
        case .referralInfo(_, let userID):
            return ["userId": userID]
            
        case .fareSetting(_ , let origin, let destination, let version):
            var params = [String: Any]()
            params["origin"] = origin
            params["destination"] = destination
            params["v"] = version
            return params
            
        case .listCard:
            return nil
            
        case .removeCard(_, let userId, let tokenId):
            var params = [String: Any]()
            params["userId"] = userId
            params["tokenId"] = tokenId
            return params
            
        case .getUserDebt:
            return nil
            
        case .payUserDebt(_, let payment, let cardId, let tripIds):
            var params = [String: Any]()
            params["payment"] = payment
            params["cardId"] = cardId
            params["tripIds"] = tripIds
            return params
        case .getFavPlaceType:
            return nil
        case .createFavPlace(_, let name, let address, let typeId, let lat, let long, let isDriver):
            var params = [String: Any]()
            params["name"] = name
            params["address"] = address
            params["typeId"] = typeId
            params["lat"] = lat
            params["lon"] = long
            params["isDriver"] = isDriver ? "true" : "false"
            return params
        case .getFavPlaceList(_, let isDriver):
            var params = [String: Any]()
            params["isDriver"] = isDriver ? "true" : "false"
            return params
        case .deleteFavPlace(_, _):
            return nil
        case .updateFavPlace(_, _, let name, let address, let typeId, let lat, let long, let isDriver):
            var params = [String: Any]()
            params["name"] = name
            params["address"] = address
            params["typeId"] = typeId
            params["lat"] = lat
            params["lon"] = long
            params["isDriver"] = isDriver ? "true" : "false"
            return params
        case .driverActiveFavMode(_, _, let coordinate):
            var params = [String: Any]()
            params["lat"] = coordinate.latitude
            params["lon"] = coordinate.longitude
            return params
        case .driverTurnOffFavMode(_, _, _):
            return nil
        case .getStatusFavMode(_):
            return nil
        case .driverChangPlaceFavMode(_, _, _):
            return nil
        case .getListServices(_, _):
            return nil
        case .getFaresServices(_, let param):
            return param.toJson()
        case .findDriverInformation:
            return nil
        case .driverUpdateOnlineStatus(_, let status):
            return status.params
        case .getDetailTrip(_, let tripId):
            var params = [String: Any]()
            params["id"] = tripId
            return params
        case .updateFCMToken(let input):
            var params = [String: Any]()
            params["deviceToken"] = input.fcmToken
            params["isDriver"] = input.driver
            return params
        case .customPath(let i):
            return i.params
        case .getUserNotification(let input):
            return input.params
        case .getMasterConfigs(_):
            return nil
        case .feedback(let input):
            return input.params
        case .createSupport(let input):
            return input.params
        case .answerFeedback(let input):
            return input.params
        case .feedbackUpdateStatus(let input):
            return input.params
        case .pickupStations(let input):
            return input.params
        case .getPickupStationDriver(let input):
            return input.params
        case .removeDriverOrderTaxiRequest(let input):
            return input.params
        case .getDriverNearby(let input):
            return input.params
        case .getRequestedDriver(let input):
            return input.params
        case .updateDriverRequest(let input):
            return input.params
        }
    }
    
    public var header: [String: String]? {
        switch self {
        case .getBalance(let token):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
            
        case .searchDriver(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
            
        case .promotion(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Cache-Control"] = "min-fresh=10"
            h["Content-Type"] = "application/json"
            return h
            
        case .promotionCancel(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .promotionList(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Cache-Control"] = "min-fresh=10"
            h["Content-Type"] = "application/json"
            return h
            
        case .promotionDetail(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Cache-Control"] = "min-fresh=10"
            h["Content-Type"] = "application/json"
            return h
            
        case .promotionSearch(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Cache-Control"] = "min-fresh=10"
            h["Content-Type"] = "application/json"
            return h
            
        case .promotionNow(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Cache-Control"] = "min-fresh=10"
            h["Content-Type"] = "application/json"
            return h
            
        case .updateDeviceToken(let token, _, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .listBank:
            return nil
            
        case .checkPin(let token):
            let h: [String:String] = [
                "x-access-token":token
            ]
            return h
            
        case .addBankInfo(let token, _, _, _, _, _):
            let h: [String:String] = [
                "x-access-token":token
            ]
            return h
            
        case .listBankInfos(let token):
            let h: [String:String] = [
                "x-access-token":token
            ]
            return h
            
        case .orderWithdraw(let token, _, _, _):
            let h: [String:String] = [
                "x-access-token":token
            ]
            return h
            
        case .checkAccount(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            return h
            
        case .createAccount(let token, _, _, _, _, _, _, _, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .updateAccount(let token, _, _, _, _, _, _, _, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .checkPhone(let token, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .checkReferralCode(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .authenticateConfig:
            return ["Content-Type" : "application/json"]
            
        case .authenticateVerifyCode:
            return ["Content-Type" : "application/json"]
            
        case .authenticateRequestCode:
            return ["Content-Type" : "application/json"]
            
        case .topUp(let token , _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .topUpMomo(let token , _, _, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .topUpNapas(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
        case .topUpDomesticAtm(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .driverWithdrawInfor(let token):
            return ["x-access-token": token]
            
        case .userTransactions(let token, _, _, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .userTopupConfig(let token):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .userTransfer(let token, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .userDetailTransaction(let token, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .findDriverForBook(let token, _, _, _, _, _, _, _, _):
            var h: [String: String] = [:]
            h["x-access-token"] = token
            h["Content-Type"] = "application/json"
            return h
            
        case .getUserInfo(let token, _):
            return ["x-access-token":token]
            
        case .bookingHistory(let token, _):
            return ["x-access-token":token]
            
        case .addDriverToBlacklist(let token, _):
            return ["x-access-token":token]
            
        case .addDriverToFavorite(let token, _):
            return ["x-access-token":token]
            
        case .removeDriverFromBlacklist(let token, _):
            return ["x-access-token":token]
            
        case .removeDriverFromFavorite(let token, _):
            return ["x-access-token":token]
            
        case .referralInfo(let token, _):
            return ["x-access-token":token]
            
        case .fareSetting(let token,_ , _, _):
            return ["x-access-token":token]
            
        case .listCard(let token):
            return ["x-access-token":token]
            
        case .removeCard(let token, _, _):
            return ["x-access-token":token]
            
        case .getUserDebt(let token):
            return ["x-access-token":token]
            
        case .payUserDebt(let token, _, _, _):
            return ["x-access-token":token]
        case .getFavPlaceType(let token):
            return ["x-access-token":token]
        case .createFavPlace(let authToken, _, _, _, _, _, _):
            return ["x-access-token":authToken]
        case .getFavPlaceList(let token, _):
            return ["x-access-token":token]
        case .deleteFavPlace(let token, _):
            return ["x-access-token":token]
        case .updateFavPlace(let token, _, _, _, _, _, _, _):
            return ["x-access-token":token]
        case .driverActiveFavMode(let authToken, _, _):
            return ["x-access-token":authToken]
        case .driverTurnOffFavMode(let authToken, _, _):
            return ["x-access-token":authToken]
        case .getStatusFavMode(let authToken):
            return ["x-access-token":authToken]
        case .driverChangPlaceFavMode(let authToken, _, _):
            return ["x-access-token":authToken]
        case .getListServices(let authToken, _):
            return ["x-access-token":authToken]
        case .getFaresServices(let authToken, _):
            return ["x-access-token":authToken]
        case .findDriverInformation(let authToken, _):
            return ["x-access-token":authToken]
        case .driverUpdateOnlineStatus(let authToken, _):
            return ["x-access-token":authToken]
        case .getDetailTrip(let authToken, _):
            return ["x-access-token":authToken]
        case .updateFCMToken(let input):
            return ["x-access-token": input.authToken]
        case .customPath(let i):
            var header = i.header ?? [:]
            header["x-access-token"] = i.authToken
            return header
        case .getUserNotification(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authToken
            h["Content-Type"] = "application/json"
            return h
        case .getMasterConfigs(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.authToken
            return h
        case .feedback(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.token
            return h
        case .createSupport(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.token
            h["token_type"] = "user"
            return h
        case .answerFeedback(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.token
            h["token_type"] = "user"
            return h
        case .feedbackUpdateStatus(let input):
            var h: [String: String] = [:]
            h["x-access-token"] = input.token
            h["token_type"] = "user"
            return h
            
        case .pickupStations(let input):
            return ["x-access-token":input.token]
        case .getPickupStationDriver(let input):
            return ["x-access-token":input.token]
        case .removeDriverOrderTaxiRequest(let input):
            return ["x-access-token":input.token]
        case .getDriverNearby(let input):
            return ["x-access-token":input.token]
        case .getRequestedDriver(let input):
            return ["x-access-token":input.token]
        case .updateDriverRequest(let input):
            return ["x-access-token":input.token]
        }
    }
}
