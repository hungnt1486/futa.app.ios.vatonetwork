//  File name   : Protocol.swift
//
//  Author      : Dung Vu
//  Created date: 11/19/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import Alamofire
import RxSwift

public protocol APIRequestProtocol: CustomStringConvertible {
    var path: String { get }
    var params: [String: Any]? { get }
    var header: [String: String]? { get }
}

extension APIRequestProtocol {
    public var description: String {
        return """
                path: \(path)
                params: \(params ?? [:])
                header: \(header ?? [:])
            """
    }
}

public typealias ProgressHandler = (Double?) -> ()
public protocol NetworkProtocol {
    static func request(using router: APIRequestProtocol, method m: HTTPMethod, encoding e: ParameterEncoding, progress: ProgressHandler?) -> Observable<(HTTPURLResponse, Data)>
}

public struct Response<T: Decodable> {
    public let httpResponse: HTTPURLResponse
    public let response: T
}

public extension NetworkProtocol {
    static func customResponse<E: Decodable>(using router: APIRequestProtocol,
                                                      method m: HTTPMethod = .get,
                                                      encoding e: ParameterEncoding = URLEncoding.default,
                                                      progress: ProgressHandler? = nil,
                                                      transform: @escaping (Data) throws -> E) -> Observable<(HTTPURLResponse, E)>
    {
        return self.request(using: router, method: m, encoding: e, progress: progress).map { ($0.0, try transform($0.1)) }
    }
    
    static func requestDTO<E: Decodable>(using router: APIRequestProtocol,
                                                method m: HTTPMethod = .get,
                                                encoding e: ParameterEncoding = URLEncoding.default,
                                                progress: ProgressHandler? = nil,
                                                block: ((JSONDecoder) -> Void)? = nil) -> Observable<(HTTPURLResponse, E)> {
        return self.customResponse(using: router,
                                   method: m,
                                   encoding: e,
                                   progress: progress,
                                   transform: { try E.toModel(from: $0, block: block) })
    }
    
    
    static func responseDTO<E: Decodable>(decodeTo type: E.Type,
                                                using router: APIRequestProtocol,
                                                method m: HTTPMethod = .get,
                                                encoding e: ParameterEncoding = URLEncoding.default,
                                                progress: ProgressHandler? = nil,
                                                block: ((JSONDecoder) -> Void)? = nil) -> Observable<Response<E>> {
        let r: Observable<(HTTPURLResponse, E)> = self.requestDTO(using: router, method: m, encoding: e,progress: progress, block: block)
        return r.map { Response(httpResponse: $0.0, response: $0.1) }
    }
    
}

// MARK: - Service
public typealias NetworkResponse = (response: HTTPURLResponse?, data: Data)
public protocol NetworkServiceProtocol {
    func request(use url: URLConvertible,
                 method: HTTPMethod,
                 parameters: Parameters?,
                 encoding: ParameterEncoding ,
                 headers: [String: String]?,
                 ignoreCache: Bool) -> Observable<Swift.Result<NetworkResponse, Error>>
}


