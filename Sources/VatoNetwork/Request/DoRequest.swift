//  File name   : DoRequest.swift
//
//  Author      : Dung Vu
//  Created date: 2/1/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 Vato. All rights reserved.
//  --------------------------------------------------------------

import RxSwift

public typealias OutputResponse<E> = Observable<Swift.Result<E, Error>>

// MARK: - Input Request
public protocol OutputProtocol {
    associatedtype Output
    func callAsFunction(_ input: RequestNetworkProtocol) -> OutputResponse<Output>
}

public struct DoRequest {
    private let networkProvider: RequestNetworkProtocol
    public init(networkProvider: RequestNetworkProtocol) {
        self.networkProvider = networkProvider
    }
    public func callAsFunction<T: OutputProtocol>(_ input: T) -> OutputResponse<T.Output> {
        input(networkProvider)
    }
}
