//  File name   : Request.swift
//
//  Author      : Dung Vu
//  Created date: 2/2/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import Alamofire
import RxSwift
import Foundation

public struct RequestAPIInput<Output: Decodable>: OutputProtocol {
    public let request: APIRequestProtocol
    public var ignoreCache: Bool
    public var method: Alamofire.HTTPMethod
    public var encoding: Alamofire.ParameterEncoding
    public var customDecoder: ((JSONDecoder) -> Void)?
    
    public init(request: APIRequestProtocol,
                ignoreCache: Bool = false,
                method: Alamofire.HTTPMethod = .get,
                encoding: Alamofire.ParameterEncoding = URLEncoding.default,
                customDecoder: ((JSONDecoder) -> Void)? = nil) {
        self.request = request
        self.ignoreCache = ignoreCache
        self.method = method
        self.encoding = encoding
        self.customDecoder = customDecoder
    }
    
    public func callAsFunction(_ input: RequestNetworkProtocol) -> OutputResponse<Output> {
        input.request(using: request,
                      decodeTo: Output.self,
                      method: method,
                      encoding: encoding,
                      ignoreCache: ignoreCache,
                      block: customDecoder)
    }
}

// MARK: - Params Request
/// Custom params
public struct ParamsRequest<T: ModifyHostProtocol> {
    public let path: BlockModifyByValue<Void, String>
    public private (set) var params: [String: Any]? = nil
    public private (set) var headers: [String: String]? = nil
    
    /// Create custom request
    /// - Parameters:
    ///   - path: block can get path require
    ///   - params: json, can be nil
    ///   - headers: custom, can be nil
    public init(path: @escaping BlockModifyByValue<Void, String>,
                params: [String: Any]? = nil,
                headers: [String: String]? = nil) {
        self.path = path
        self.params = params
        self.headers = headers
        
    }
    
    public subscript(p key: String) -> Any? {
        get {
            params?[key]
        }
        
        set {
            if params == nil {
                params = [String: Any]()
            }
            params?[key] = newValue
        }
     }
    
    public subscript(h key: String) -> String? {
        get {
            headers?[key]
        }
        
        set {
            if headers == nil {
                headers = [String: String]()
            }
            headers?[key] = newValue
        }
     }
    
    public func callAsFunction() -> APIRequestProtocol {
        let p: String = T.path(path(()))
        return VatoAPIRouter.customPath(authToken: "",
                                        path: p,
                                        header: headers,
                                        params: params,
                                        useFullPath: T.useFullPath)
    }
}
