//  File name   : HostProtocol.swift
//
//  Author      : Dung Vu
//  Created date: 2/2/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

public typealias BlockModifyByValue<Value, T> = (Value) -> T

// MARK: - Custom Host

/// Protocol for purpose use custom host
public protocol ModifyHostProtocol {
    
    /// Host that url
    static var host: String { get }
    
    /// Block uses for append path
    static var path: BlockModifyByValue<String, String> { get }
    
    /// Make vato framework use default host
    static var useFullPath: Bool { get }
}

public extension ModifyHostProtocol {
    static var path: BlockModifyByValue<String, String> {
        return { host + $0 }
    }
    
    static var useFullPath: Bool {
        true
    }
}

// MARK: - Extension conform
extension VatoFoodApi: ModifyHostProtocol {}
extension VatoTicketApi: ModifyHostProtocol {}

/// Default host vato
public struct DefaultHost: ModifyHostProtocol {
    public static var host: String { "" }
    public static var path: BlockModifyByValue<String, String> = { p in
        return p
    }
    
    public static var useFullPath: Bool { false }
}

