//  File name   : NetworkProvider.swift
//
//  Author      : Dung Vu
//  Created date: 2/2/21
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2021 FUTA Group. All rights reserved.
//  --------------------------------------------------------------

import Alamofire
import RxSwift

// MARK: - Excute Request
public struct EmptyAPINetwork: RequestNetworkProtocol {
    public struct Request: NetworkServiceProtocol {
        public func request(use url: URLConvertible,
                     method: Alamofire.HTTPMethod,
                     parameters: Parameters?,
                     encoding: ParameterEncoding,
                     headers: [String : String]?,
                     ignoreCache: Bool) -> Observable<Result<NetworkResponse, Error>>
        {
            Observable.empty()
        }
        public init() {}
    }
    
    public let provider: NetworkServiceProtocol
    public init(provider: NetworkServiceProtocol = Request()) {
        self.provider = provider
    }
}


