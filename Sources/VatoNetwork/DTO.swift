//  File name   : DTO.swift
//
//  Author      : Dung Vu
//  Created date: 11/19/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation

public protocol NetworkResponseStatusProtocol {
    associatedtype Model: Codable
    
    var message: String? { get }
    var errorCode: String? { get }
    var status: Int { get }
    var data: Model { get }
}

public extension NetworkResponseStatusProtocol {
    var fail: Bool {
        return status != 200
    }
    
    var error: Error? {
        guard fail else {
            return nil
        }
        
        return NSError(domain: NSURLErrorDomain, code: status, userInfo: [NSLocalizedDescriptionKey : errorCode ?? ""])
    }
}

public struct MessageDTO<T: Codable>: Codable, NetworkResponseStatusProtocol {
    public var message: String?
    public var errorCode: String?
    public var status: Int
    public var data: T
}

public struct OptionalMessageDTO<T: Codable>: Codable, NetworkResponseStatusProtocol {
    public typealias Model = Optional<T>
    public var message: String?
    public var errorCode: String?
    public var status: Int
    public var data: Model
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case errorCode = "errorCode"
        case status = "status"
        case data = "data"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        errorCode = try values.decodeIfPresent(String.self, forKey: .errorCode)
        status = try values.decode(Int.self, forKey: .status)
        data = try values.decodeIfPresent(T.self, forKey: .data)
    }
}

public struct OptionalIgnoreMessageDTO<T: Codable>: Codable, NetworkResponseStatusProtocol {
    public typealias Model = Optional<T>
    public var message: String?
    public var errorCode: String?
    public var status: Int
    public var data: Model
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case errorCode = "errorCode"
        case status = "status"
        case data = "data"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        errorCode = try values.decodeIfPresent(String.self, forKey: .errorCode)
        status = try values.decode(Int.self, forKey: .status)
        
        if let value = try? values.decode(T.self, forKey: .data) {
            data = value
        } else {
            data = nil
        }

    }
}
