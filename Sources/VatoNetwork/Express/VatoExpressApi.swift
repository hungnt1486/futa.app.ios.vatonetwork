//  File name   : VatoTicketApi.swift
//
//  Author      : Dung Vu
//  Created date: 9/23/19
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2019 Dung Vu. All rights reserved.
//  --------------------------------------------------------------

import Foundation

public enum VatoExpressApi: APIRequestProtocol {
    private static let pathDev = "https://api-express-dev.vato.vn/api"
    private static let pathProduction = "https://api-express.vato.vn/api"
    
    private var host: String {
        return API.debugging ? VatoExpressApi.pathDev : VatoExpressApi.pathProduction
    }
    
    case listTimeAvailableDelivery(authToken: String, date: String)
    
    public var path: String {
        switch self {
        case .listTimeAvailableDelivery(_ , let date):
            return "\(host)/orders/pickup-time/\(date)"
        }
    }
    
    public var params: [String : Any]? {
        switch self {
        case .listTimeAvailableDelivery(_, _):
            return nil
        }
    }
    
    public var header: [String : String]? {
        switch self {
        case .listTimeAvailableDelivery(let authToken, _):
            return ["x-access-token":authToken]
        }
    }
}
