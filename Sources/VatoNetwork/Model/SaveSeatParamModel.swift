//
//  SaveSeatParamModel.swift
//  VatoNetwork
//
//  Created by THAI LE QUANG on 10/10/19.
//  Copyright © 2019 Dung Vu. All rights reserved.
//

import UIKit

//{
//    "CarBookingId": "string",
//    "CustAddress": "string",
//    "CustBirthDay": "2019-10-10T08:07:03.692Z",
//    "CustCity": "string",
//    "CustCode": "string",
//    "CustCountry": "string",
//    "CustEmail": "string",
//    "CustId": 0,
//    "CustMobile": "string",
//    "CustMobile2": "string",
//    "CustName": "string",
//    "CustSN": "string",
//    "DepartureDate": "string",
//    "DepartureTime": "string",
//    "EnglishTicket": 0,
//    "Locale": "string",
//    "NumOfTicket": 0,
//    "OfficePickupId": 0,
//    "Passengers": [
//    {
//    "CustMobile": "string",
//    "CustName": "string",
//    "CustSN": "string"
//    }
//    ],
//    "PickUpStreet": "string",
//    "RouteId": 0,
//    "RouteName": "string",
//    "SeatDiscounts": [
//    0
//    ],
//    "SeatIds": [
//    0
//    ],
//    "SeatNames": [
//    "string"
//    ],
//    "Version": 0,
//    "custMobile2": "string",
//    "custState": "string"
//}

public struct PassengersModel: Codable {
    public let custMobile: String?
    public let custMobile2: String?
    public let custName: String?
    public let custSN: String?
    public let custEmail: String?
    public let custCMND: String?
    
    public init(custMobile: String?,custMobile2: String?,custName: String?,custSN: String?,custEmail: String?,custCMND: String?) {
        self.custMobile = custMobile
        self.custMobile2 = custMobile2
        self.custName = custName
        self.custSN = custSN
        self.custEmail = custEmail
        self.custCMND = custCMND
    }
    
    public func toJson() -> [String: Any] {
        let params: [String: Any] = [
            "CustMobile": self.custMobile ?? "",
            "CustMobile2": self.custMobile2 ?? "",
            "CustName": self.custName ?? "",
            "CustSN": self.custSN ?? "",
            "CustEmail": self.custEmail ?? "",
            "CustCMND": self.custCMND ?? "",
        ]
        
        return params
    }
    
    public enum CodingKeys: String, CodingKey {
        
        case custMobile = "custMobile"
        case custMobile2 = "custMobile2"
        case custName = "custName"
        case custSN = "custSN"
        case custEmail = "custEmail"
        case custCMND = "custCMND"
        
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        custMobile = try values.decodeIfPresent(String.self, forKey: .custMobile)
        custMobile2 = try values.decodeIfPresent(String.self, forKey: .custMobile2)
        custName = try values.decodeIfPresent(String.self, forKey: .custName)
        custSN = try values.decodeIfPresent(String.self, forKey: .custSN)
        custEmail = try values.decodeIfPresent(String.self, forKey: .custEmail)
        custCMND = try values.decodeIfPresent(String.self, forKey: .custCMND)
    }
}

public struct SaveSeatParamModel: Codable {

    public let carBookingId: String?
    public let custAddress: String?
    public let custBirthDay: String?
    public let custCity: String?
    public let custCode: String?
    public let custCountry: String?
    public let custEmail: String?
    public let custId: String?
    public let custMobile: String?
    public let custMobile2: String?
    public let custName: String?
    public let custSN: String?
    public let departureDate: String?
    public let departureTime: String?
    public let englishTicket: Int?
    public let locale: String?
    public let numOfTicket: Int?
    public let officePickupId: Int?
    public let passengers: [PassengersModel]?
    public let pickUpStreet: String?
    public let routeId: Int?
    public let routeName: String?
    public let seatDiscounts: [Double]?
    public let seatIds: [Int32]?
    public let seatNames: [String]?
    public let version: Int?
    public let custState: String?
    public let destCode: String?
    public let destName: String?
    public let originCode: String?
    public let originName: String?
    public let price: Int64?
    public let wayId: Int32?
    public let pickUpName: String?
    public let distance: Double?
    public let duration: Double?
    public let kind: String?
    
    public let code: String?
    public let custCMND: String?
    public var timeExpiredPayment: TimeInterval?
    
    public init(carBookingId: String?,
                custAddress: String?,
                custBirthDay: String?,
                custCity: String?,
                custCode: String?,
                custCountry: String?,
                custEmail: String?,
                custId: String?,
                custMobile: String?,
                custMobile2: String?,
                custName: String?,
                custSN: String?,
                departureDate: String?,
                departureTime: String?,
                englishTicket: Int?,
                locale: String?,
                numOfTicket: Int?,
                officePickupId: Int?,
                passengers: [PassengersModel]?,
                pickUpStreet: String?,
                routeId: Int?,
                routeName: String?,
                seatDiscounts: [Double]?,
                seatIds: [Int32]?,
                seatNames: [String]?,
                version: Int?,
                custState: String?,
                destCode: String?,
                destName: String?,
                originCode: String?,
                originName: String?,
                price: Int64?,
                pickUpName: String?,
                distance: Double?,
                duration: Double?,
                wayId: Int32?,
                kind: String?,
                custCMND: String?
        ) {
        
        self.carBookingId = carBookingId
        self.custAddress = custAddress
        self.custBirthDay = custBirthDay
        self.custCity = custCity
        self.custCode = custCode
        self.custCountry = custCountry
        self.custEmail = custEmail
        self.custId = custId
        self.custMobile = custMobile
        self.custMobile2 = custMobile2
        self.custName = custName
        self.custSN = custSN
        self.departureDate = departureDate
        self.departureTime = departureTime
        self.englishTicket = englishTicket
        self.locale = locale
        self.numOfTicket = numOfTicket
        self.officePickupId = officePickupId
        self.passengers = passengers
        self.pickUpStreet = pickUpStreet
        self.routeId = routeId
        self.routeName = routeName
        self.seatDiscounts = seatDiscounts
        self.seatIds = seatIds
        self.seatNames = seatNames
        self.version = version
        self.custState = custState
        self.destCode = destCode
        self.destName = destName
        self.originCode = originCode
        self.originName = originName
        self.price = price
        self.code = ""
        self.pickUpName = pickUpName
        self.distance = distance
        self.duration = duration
        self.wayId = wayId
        self.kind = kind
        self.custCMND = custCMND
    }
    
    public func toJson() -> [String: Any] {
        let passengersDic = self.passengers?.compactMap { $0.toJson() }
        
        let params: [String: Any] = [
            "CarBookingId": self.carBookingId ?? "",
            "CustAddress": self.custAddress ?? "",
            "CustBirthDay": self.custBirthDay ?? "",
            "CustCity": self.custCity ?? "",
            "CustCode": self.custCode ?? "",
            "CustCountry": self.custCountry ?? "",
            "CustEmail": self.custEmail ?? "",
            "CustId": self.custId ?? "",
            "CustMobile": self.custMobile ?? "",
            "CustMobile2": self.custMobile2 ?? "",
            "CustName": self.custName ?? "",
            "CustSN": self.custSN ?? "",
            "DepartureDate": self.departureDate ?? "",
            "DepartureTime": self.departureTime ?? "",
            "EnglishTicket": self.englishTicket ?? "",
            "Locale": self.locale ?? "",
            "NumOfTicket": self.numOfTicket ?? "",
            "OfficePickupId": self.officePickupId ?? "",
            "Passengers": passengersDic ?? [],
            "PickUpStreet": self.pickUpStreet ?? "",
            "RouteId": self.routeId ?? "",
            "RouteName": self.routeName ?? "",
            "SeatDiscounts": self.seatDiscounts ?? [],
            "SeatIds": self.seatIds ?? "",
            "SeatNames": self.seatNames ?? "",
            "Version": self.version ?? "",
            "CustState": self.custState ?? "",
            "DestCode": self.destCode ?? "",
            "DestName": self.destName ?? "",
            "OriginCode": self.originCode ?? "",
            "OriginName": self.originName ?? "",
            "Price": self.price ?? 0,
            "PickUpName": self.pickUpName ?? "",
            "Distance": self.distance ?? 0,
            "Duration": self.duration ?? 0,
            "WayId": self.wayId ?? 0,
            "Kind": self.kind ?? "",
            "CustCMND": self.custCMND ?? ""
        ]
        
        return params
    }
    
    public enum CodingKeys: String, CodingKey {
        
        case carBookingId = "carBookingId"
        case custAddress = "custAddress"
        case custBirthDay = "custBirthDay"
        case custCity = "custCity"
        case custCode = "custCode"
        case custCountry = "custCountry"
        case custEmail = "custEmail"
        case custId = "custId"
        case custMobile = "custMobile"
        case custMobile2 = "custMobile2"
        case custName = "custName"
        case custSN = "custSN"
        case departureDate = "departureDate"
        case departureTime = "departureTime"
        case englishTicket = "englishTicket"
        case locale = "locale"
        case numOfTicket = "numOfTicket"
        case officePickupId = "officePickupId"
        case passengers = "passengers"
        case pickUpStreet = "pickUpStreet"
        case routeId = "routeId"
        case routeName = "routeName"
        case seatDiscounts = "seatDiscounts"
        case seatIds = "seatIds"
        case seatNames = "seatNames"
        case version = "version"
        case custState = "custState"
        case originCode = "originCode"
        case originName = "originName"
        case destCode = "destCode"
        case destName = "destName"
        case price = "price"
        case code = "code"
        case pickUpName = "pickUpName"
        case distance = "distance"
        case duration = "duration"
        case timeExpiredPayment = "timeExpiredPayment"
        case wayId = "wayId"
        case kind = "kind"
        case custCMND = "custCMND"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.carBookingId = try values.decodeIfPresent(String.self, forKey: .carBookingId)
        self.custAddress = try values.decodeIfPresent(String.self, forKey: .custAddress)
        self.custBirthDay = try values.decodeIfPresent(String.self, forKey: .custBirthDay)
        self.custCity = try values.decodeIfPresent(String.self, forKey: .custCity)
        self.custCode = try values.decodeIfPresent(String.self, forKey: .custCode)
        self.custCountry = try values.decodeIfPresent(String.self, forKey: .custCountry)
        self.custEmail = try values.decodeIfPresent(String.self, forKey: .custEmail)
        self.custId = try values.decodeIfPresent(String.self, forKey: .custId)
        self.custMobile = try values.decodeIfPresent(String.self, forKey: .custMobile)
        self.custMobile2 = try values.decodeIfPresent(String.self, forKey: .custMobile2)
        self.custName = try values.decodeIfPresent(String.self, forKey: .custName)
        self.custSN = try values.decodeIfPresent(String.self, forKey: .custSN)
        self.departureDate = try values.decodeIfPresent(String.self, forKey: .departureDate)
        self.departureTime = try values.decodeIfPresent(String.self, forKey: .departureTime)
        self.englishTicket = try values.decodeIfPresent(Int.self, forKey: .englishTicket)
        self.locale = try values.decodeIfPresent(String.self, forKey: .locale)
        self.numOfTicket = try values.decodeIfPresent(Int.self, forKey: .numOfTicket)
        self.officePickupId = try values.decodeIfPresent(Int.self, forKey: .officePickupId)
        self.passengers = try values.decodeIfPresent([PassengersModel].self, forKey: .passengers)
        self.pickUpStreet = try values.decodeIfPresent(String.self, forKey: .pickUpStreet)
        self.routeId = try values.decodeIfPresent(Int.self, forKey: .routeId)
        self.routeName = try values.decodeIfPresent(String.self, forKey: .routeName)
        self.seatDiscounts = try values.decodeIfPresent([Double].self, forKey: .seatDiscounts)
        self.seatIds = try values.decodeIfPresent([Int32].self, forKey: .seatIds)
        self.seatNames = try values.decodeIfPresent([String].self, forKey: .seatNames)
        self.version = try values.decodeIfPresent(Int.self, forKey: .version)
        self.custState = try values.decodeIfPresent(String.self, forKey: .custState)
        self.originCode = try values.decodeIfPresent(String.self, forKey: .originCode)
        self.originName = try values.decodeIfPresent(String.self, forKey: .originName)
        self.destCode = try values.decodeIfPresent(String.self, forKey: .destCode)
        self.destName = try values.decodeIfPresent(String.self, forKey: .destName)
        self.price = try values.decodeIfPresent(Int64.self, forKey: .price)
        self.code = try values.decodeIfPresent(String.self, forKey: .code)
        self.pickUpName = try values.decodeIfPresent(String.self, forKey: .pickUpName)
        self.distance = try values.decodeIfPresent(Double.self, forKey: .distance)
        self.duration = try values.decodeIfPresent(Double.self, forKey: .duration)
        self.timeExpiredPayment = try values.decodeIfPresent(TimeInterval.self, forKey: .timeExpiredPayment)
        self.wayId = try values.decodeIfPresent(Int32.self, forKey: .wayId)
        self.kind = try values.decodeIfPresent(String.self, forKey: .kind)
        self.custCMND = try values.decodeIfPresent(String.self, forKey: .custCMND)
    }
}
