//
//  ParamModel.swift
//  VatoNetwork
//
//  Created by vato. on 9/13/19.
//  Copyright © 2019 Dung Vu. All rights reserved.
//

import Foundation

// MARK: -- Fare
public struct FareServiceParam  {
    let trip_type : Int
    let zone_id : Int?
    let distance : Int
    let duration : Int
    let start_lat : Double
    let start_lon : Double
    let end_lat : Double
    let end_lon : Double
    let service_ids : [Int]
    
    let addInfo: [String: Any]?
    
   public init(trip_type : Int,
        zone_id : Int?,
        distance : Int,
        duration : Int,
        start_lat : Double,
        start_lon : Double,
        end_lat : Double,
        end_lon : Double,
        service_ids : [Int] ,
        addInfo: [String: Any]?) {
        self.trip_type = trip_type
        self.zone_id = zone_id
        self.distance = distance
        self.duration = duration
        self.start_lat = start_lat
        self.start_lon = start_lon
        self.end_lat = end_lat
        self.end_lon = end_lon
        self.service_ids = service_ids
        self.addInfo = addInfo
    }
    
   public func toJson() -> [String: Any] {
    var params: [String: Any] = [
        "trip_type": self.trip_type,
        "distance": self.distance,
        "duration": self.duration,
        "start_lat": self.start_lat,
        "start_lon": self.start_lon,
        "end_lat": self.end_lat,
        "end_lon": self.end_lon,
        "service_ids": self.service_ids]
        params["zone_id"] = zone_id
        if let json = addInfo {
           params += json
        }
        return params
    }
}

protocol ExportParamsProtocol {
    var params: JSON? { get }
}

extension ExportParamsProtocol where Self: Encodable {
    var params: JSON?  {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(self)
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? JSON
            print("!!! params: \(json ?? [:])")
            return json
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}

// MARK: -- Driver online status
public struct DriverOnlineParam: Codable, ExportParamsProtocol {
    let status: Int
    let location: MapModel.Location
}

