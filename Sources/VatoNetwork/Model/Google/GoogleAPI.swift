//  File name   : GoogleApi.swift
//
//  Author      : Vato
//  Created date: 10/22/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Vato. All rights reserved.
//  --------------------------------------------------------------

import Foundation
import CoreLocation
import Alamofire
import RxSwift

#if canImport(GoogleMaps)
    import GoogleMaps
#endif

public struct GoogleAPI {
    /// Class's public properties.
    public enum Router: APIRequestProtocol {
        // mode: driving, walking, bicycling
        case direction(location1: CLLocationCoordinate2D, location2: CLLocationCoordinate2D?, mode: String?)
        case geocoding(location: CLLocationCoordinate2D)
        case place(keyword: String, location: CLLocationCoordinate2D)
        case placeDetail(placeID: String)

        public var path: String {
            switch self {
            case .direction:
                return "\(GoogleAPI.domain)/maps/api/directions/json"
            case .geocoding:
                return "\(GoogleAPI.domain)/maps/api/geocode/json"
            case .place:
                return "\(GoogleAPI.domain)/maps/api/place/autocomplete/json"
            case .placeDetail:
                return "\(GoogleAPI.domain)/maps/api/place/details/json"
            }
        }

        public var params: [String: Any]? {
            switch self {
            case .direction(let location1, let location2, let mode):
                guard !GoogleAPI.currentToken.isEmpty else {
                    fatalError("Google key must not empty.")
                }
                var result: [String:Any] = [
                    "mode": mode ?? "driving",
                    "origin":location1.value,
                    "key":GoogleAPI.currentToken
                ]

                result["destination"] = location2?.value
                return result

            case .geocoding(let location):
                guard !GoogleAPI.currentToken.isEmpty else {
                    fatalError("Google key must not empty.")
                }
                let params: [String:Any] = [
                    "latlng":String(format: "%f,%f", location.latitude, location.longitude),
                    "components":"street_address|route",
                    "language":"vi",
                    "key":GoogleAPI.currentToken
                ]
                return params

            case .place(let keyword, let location):
                guard !GoogleAPI.currentToken.isEmpty else {
                    fatalError("Google key must not empty.")
                }
                let params: [String:Any] = [
                    "strictbounds":"",
                    "language":"vi",
                    "location":String(format: "%f,%f", location.latitude, location.longitude),
                    "components":"country:VN",
//                    "types":"address",
                    "input":keyword,
                    "radius":100000,
                    "key":GoogleAPI.currentToken
                ]
                return params

            case .placeDetail(let placeID):
                guard !GoogleAPI.currentToken.isEmpty else {
                    fatalError("Google key must not empty.")
                }
                let params: [String:Any] = [
                    "placeid":placeID,
                    "key":GoogleAPI.currentToken
                ]
                return params
            }
        }

        public var header: [String: String]? {
            return nil
        }
    }

    /// Update Google API tokens.
    public static func update(tokens: [String]) {
        self.tokens = tokens
        switchToken()
    }

    fileprivate static func switchToken() {
        guard tokens.count > 0 else {
            return
        }

        let token = tokens.removeLast()
        currentToken = token
    }

    /// Class's private properties.
    private static var domain: String {
        return "https://maps.googleapis.com"
    }

    private static var currentToken = ""
    private static var tokens: [String] = []
    private static var expiredTokens: [String] = []
}

// MARK: Google REST API
public extension GoogleAPI {
    static func reverseGeocode(with coordinate: CLLocationCoordinate2D, googleAPIKey: String) -> Observable<GoogleModel.Result?> {
        let o: Observable<(HTTPURLResponse, GoogleModel.Geocoding)> = Requester.requestDTO(using: Router.geocoding(location: coordinate),
                                                                                           method: .get,
                                                                                           encoding: URLEncoding.default,
                                                                                           block: nil)

        return o.map { $0.1.results }
            .map { results -> GoogleModel.Result? in
                let array: [GoogleModel.Result] = results.filter { $0.geometry.isValid }

                var array2 = array.map { result -> (Int, Double, GoogleModel.Result) in
                    let index = GOOGLE_GEOMETRY_PRIORITIES.firstIndex(of: result.geometry.locationType ?? "") ?? 10              // If we could not find the index, ignore this record.

                    #if canImport(GoogleMaps)
                        let coord = CLLocationCoordinate2D(latitude: result.geometry.location.lat, longitude: result.geometry.location.lng)
                        let distance = GMSGeometryDistance(coordinate, coord)
                        return (index, distance, result)
                    #else
                        let origin = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                        let coord = CLLocation(latitude: result.geometry.location.lat, longitude: result.geometry.location.lng)
                        let distance = coord.distance(from: origin)
                        return (index, distance, result)
                    #endif
                }

                array2.sort(by: { (item1, item2) -> Bool in
                    if item1.0 < item2.0 {
                        return true
                    } else if item1.0 > item2.0 {
                        return false
                    } else {
                        return item1.1 < item2.1
                    }
                })

                return array2.first(where: { $0.2.addressComponents.count >= 4 && $0.2.addressComponents.first(where: { $0.types.contains("route") }) != nil })?.2
            }
            .do(onError: { (_) in switchToken() })
    }

    static func placeAutoComplete(with keyword: String, currentLocation: CLLocationCoordinate2D, googleAPIKey: String) -> Observable<[GoogleModel.Prediction]> {
        let o: Observable<(HTTPURLResponse, GoogleModel.Place)> = Requester.requestDTO(using: Router.place(keyword: keyword, location: currentLocation),
                                                                                       method: .get,
                                                                                       encoding: URLEncoding.default,
                                                                                       block: nil)

        return o.map { $0.1.predictions }.do(onError: { (_) in switchToken() })
    }

    static func placeDetails(with placeID: String, googleAPIKey: String) -> Observable<GoogleModel.Result> {
        let o: Observable<(HTTPURLResponse, GoogleModel.PlaceDetail)> = Requester.requestDTO(using: Router.placeDetail(placeID: placeID),
                                                                                             method: .get,
                                                                                             encoding: URLEncoding.default,
                                                                                             block: nil)

        return o.map { $0.1.result }.do(onError: { (_) in switchToken() })
    }
}

// MARK: Decode polyline
#if canImport(GoogleMaps)
public extension GoogleAPI {
    public static func decode(polyline: String?) -> [CLLocationCoordinate2D] {
        guard
            let polyline = polyline,
            let path = GMSPath(fromEncodedPath: polyline), path.count() > 0
            else {
                return []
        }

        let coordinates = (0..<path.count()).map { path.coordinate(at: $0) }
        return coordinates
    }
}
#endif

// MARK: Encode polyline
public extension GoogleAPI {
    static func encodePolyline(with coordinates: [CLLocation]) -> String {
        return encodePolyline(with: coordinates.map { $0.coordinate })
    }

    static func encodePolyline(with coordinates: [CLLocationCoordinate2D]) -> String {
        guard coordinates.count > 0 else {
            return ""
        }

        var previousCoord = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
        var scalars: [Unicode.Scalar?] = []

        for coord in coordinates {
            // Encode latitude
            var valLat = Int(round((coord.latitude - previousCoord.latitude) * 0x1e5))
            valLat = (valLat < 0) ? ~(valLat << 1) : (valLat << 1)
            while valLat >= 0x20 {
                let value = (0x20 | (valLat & 31)) + 63
                scalars.append(Unicode.Scalar(value))

                valLat >>= 5
            }
            scalars.append(Unicode.Scalar(valLat + 63))

            // Encode longitude
            var valLng = Int(round((coord.longitude - previousCoord.longitude) * 1e5))
            valLng = (valLng < 0) ? ~(valLng << 1) : (valLng << 1)
            while valLng >= 0x20 {
                let value = (0x20 | (valLng & 31)) + 63
                scalars.append(Unicode.Scalar(value))

                valLng >>= 5
            }
            scalars.append(Unicode.Scalar(valLng + 63))

            // Next coord
            previousCoord = coord
        }

        let chars = scalars.compactMap { $0 }.map { Character($0) }
        return String(chars)
    }
}
