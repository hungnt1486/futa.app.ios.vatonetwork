//  File name   : GoogleGeocoding.swift
//
//  Author      : Vato
//  Created date: 10/23/18
//  Version     : 1.00
//  --------------------------------------------------------------
//  Copyright © 2018 Vato. All rights reserved.
//  --------------------------------------------------------------

import Foundation

let GOOGLE_GEOMETRY_PRIORITIES = ["ROOFTOP", "RANGE_INTERPOLATED", "GEOMETRIC_CENTER"]

public struct GoogleModel {
    public struct AddressComponent: Codable {
        public let longName: String
        public let shortName: String
        public let types: [String]

        private enum CodingKeys: String, CodingKey {
            case longName = "long_name"
            case shortName = "short_name"
            case types
        }
    }

    public struct Geometry: Codable {
        public let location: Location
        public var locationType: String?
//        public let viewport: Viewport
//        public let bounds: Bounds?

        public var isValid: Bool {
            guard let locationType = locationType else {
                return false
            }
            return GOOGLE_GEOMETRY_PRIORITIES.contains(locationType)
        }

        private enum CodingKeys: String, CodingKey {
            case location
            case locationType = "location_type"
//            case viewport
//            case bounds
        }
    }

    public struct Geocoding: Codable {
//        public let plusCode: PlusCode
        public let results: [Result]
        public let status: String

        private enum CodingKeys: String, CodingKey {
//            case plusCode = "plus_code"
            case results
            case status
        }
    }

    public struct Location: Codable {
        public let lat: Double
        public let lng: Double
    }

    public struct MainTextMatchedSubstring: Codable {
        public let length: Int
        public let offset: Int
    }

    public struct MatchedSubstring: Codable {
        public let length: Int
        public let offset: Int
    }

    public struct Place: Codable {
        public let predictions: [Prediction]
        public let status: String
    }

    public struct PlaceDetail: Codable {
        public let result: Result
        public let status: String

        private enum CodingKeys: String, CodingKey {
            case result
            case status
        }
    }

    public struct PlusCode: Codable {
        public let compoundCode: String
        public let globalCode: String

        private enum CodingKeys: String, CodingKey {
            case compoundCode = "compound_code"
            case globalCode = "global_code"
        }
    }

    public struct Prediction: Codable {
        public let description: String
        public let id: String
        public let matchedSubstrings: [MatchedSubstring]
        public let placeId: String
        public let reference: String
        public let structuredFormatting: StructuredFormatting
        public let terms: [Term]
        public let types: [String]

        private enum CodingKeys: String, CodingKey {
            case description
            case id
            case matchedSubstrings = "matched_substrings"
            case placeId = "place_id"
            case reference
            case structuredFormatting = "structured_formatting"
            case terms
            case types
        }
    }

    public struct Result: Codable {
        public let addressComponents: [AddressComponent]
        public let formattedAddress: String
        public let geometry: Geometry
//        public let icon: URL
//        public let id: String
        public var name: String?
        public var placeId: String?
//        public let placeId: String
//        public let plusCode: PlusCode
//        public let reference: String
//        public let scope: String
//        public let types: [String]
//        public let url: URL
//        public let utcOffset: Int
//        public let plusCode: PlusCode?
        public var types: [String] = []

        private enum CodingKeys: String, CodingKey {
            case addressComponents = "address_components"
            case formattedAddress = "formatted_address"
            case geometry
//            case icon
//            case id
            case name
            case placeId = "place_id"
//            case plusCode = "plus_code"
//            case reference
//            case scope
//            case types
//            case url
//            case utcOffset = "utc_offset"
//            case plusCode = "plus_code"
            case types
        }
    }

    public struct StructuredFormatting: Codable {
        public let mainText: String
        public let mainTextMatchedSubstrings: [MainTextMatchedSubstring]
        public let secondaryText: String

        private enum CodingKeys: String, CodingKey {
            case mainText = "main_text"
            case mainTextMatchedSubstrings = "main_text_matched_substrings"
            case secondaryText = "secondary_text"
        }
    }

    public struct Term: Codable {
        public let offset: Int
        public let value: String
    }

    public struct Bounds: Codable {
        public let northeast: Northeast
        public let southwest: Southwest
    }
    public struct Viewport: Codable {
        public let northeast: Northeast
        public let southwest: Southwest
    }
    public struct Southwest: Codable {
        public let lat: Double
        public let lng: Double
    }
    public struct Northeast: Codable {
        public let lat: Double
        public let lng: Double
    }
}
