//
//  LogResponse.swift
//  VatoNetwork
//
//  Created by Dung Vu on 12/22/20.
//  Copyright © 2020 Dung Vu. All rights reserved.
//

import Foundation
import Alamofire

typealias LogResponseInput = (_ request: DataRequest, _ response: Data?, _ error: Error?) -> Void
struct LogResponse {
    static let queue = DispatchQueue(label: "com.VatoNetwork.logResponse")
    static func log(input: APIRequestProtocol) -> LogResponseInput {
        return { request, response, error in
            guard API.logAPI else { return }
             
            queue.async(execute: {
                var textError = ""
                if let e = error {
                    textError = """
                                🐞 Error:
                                \(e.localizedDescription)
                                """
                }
                
                var res: String?
                if let data = response {
                    res = String(data: data, encoding: .utf8)
                }
                let text = """
                            ======================
                            ℹ️ Input:
                            \(input.description)

                            🔥 Response:
                            \(res ?? "")
                            \(textError)

                            ⚠️ CURL:
                            \(request.cURLDescription())
                            ======================
                        """
                print(text)
            })
        }
    }
    
    static func log(url: URLConvertible,
                    parameters: Parameters?,
                    header: [String: String]?) -> LogResponseInput
    {
        var p = ""
        if let url = try? url.asURL() {
            p = url.absoluteString
        }
        let router = VatoAPIRouter.customPath(authToken: "",
                                              path: p,
                                              header: header,
                                              params: parameters,
                                              useFullPath: true)
        return log(input: router)
    }
}
